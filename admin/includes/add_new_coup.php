<div id="modal_theme_add_coup" class="modal fade bs-example-modal-lg-coup"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				
				<h4 class="modal-title" id="myLargeModalLabel">Create Coupon</h4>
			</div>
			<div class="modal-body">
			   
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Offer Name</label>
								<input type="text" id="offer_name" name="offer_name" class="form-control" >
							</div>
						</div>
					
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Start Date: </label>
								 <input class="form-control" type="date" name="startdate" id="startdate">
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>End Date: </label>
								 <input class="form-control" type="date" name="enddate" id="enddate">
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Location: </label>
								<input type="text" id="location_city" name="location_city" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Total Discount: </label>
								<input type="text" id="discount" name="discount" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Terms and Condtions</label>
								<textarea type="text" id="coup_des" name="coup_des" class="form-control" ></textarea>
							</div>
						</div>
					</div>
					
			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveCoup()" class="btn btn-primary" >Save</button>
				<button type="button" onclick="closeModol()"class="btn btn-warning"  data-dismiss="modal" >Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->