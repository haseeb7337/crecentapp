<div id="modal_theme_add_coup" class="modal fade bs-coups-modal-lg-coup"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				
				<h4 class="modal-title" id="myLargeModalLabel">View Coupons</h4>
			</div>
			<div class="modal-body">
			   
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table id="copuonsViewTable"  class="table table-hover table-centered m-0">
									<thead>
									<tr>
										<th>Vouhcer Number</th>
										<th>Offer Name</th>
										<th>Valid Upto</th>
										<th>Location</th>
										<th>Discount</th>
										<th>Action</th>
									</tr>
									</thead>
									
								</table>
							</div>
						</div>
					
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->