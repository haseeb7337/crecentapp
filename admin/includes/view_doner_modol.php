<div id="modal_theme_add_venue" class="modal fade bs-example-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				
				<h4 class="modal-title" id="myLargeModalLabel">Doner Information</h4>
			</div>
			<div class="modal-body">
			   
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label> First Name: </label>
								<input type="text" id="first_name" name="first_name" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Last Name: </label>
								<input type="text" id="last_name" name="last_name" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Mobile : </label>
								<input type="text" id="mobile" name="mobile" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Home Phone: </label>
								<input type="text" id="home_phone" name="home_phone" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Email: </label>
								<input type="text" id="doner_email" name="doner_email" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Country: </label>
								<input type="text" id="doner_country" name="doner_country" class="form-control" >
							</div>
						</div>
					</div>
						
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>State: </label>
								<input type="text" id="doner_state" name="doner_state" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>City: </label>
								<input type="text" id="doner_city" name="doner_city" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Street Address: </label>
								<input type="text" id="doner_address" name="doner_address" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>User Role  </label>
								<input type="text" id="doner_role" name="doner_role" class="form-control" >
							</div>
						</div>
					</div>

			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveVenue()" class="btn btn-primary" >Save</button>
				<button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->