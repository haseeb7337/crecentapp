<div id="modal_theme_add_venue" class="modal fade bs-example-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				
				<h4 class="modal-title" id="myLargeModalLabel">Add New Venue</h4>
			</div>
			<div class="modal-body">
			   
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Venue Name</label>
								<input type="text" id="venue_name" name="name" class="form-control" >
							</div>
						</div>	
						<form id ="form_id" action = "DEFAULT_ACTION">	
							<div class="col-md-6">
								<div class="form-group">
									<label>Type:</label>
									<select class="select" id="addVenueType" data-init-plugin="select2">
																			
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Description</label>
								<textarea type="text" id="venue_decp" name="name" class="form-control" ></textarea>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>City: </label>
								<input type="text" id="venue_city" name="name" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>State: </label>
								<input type="text" id="venue_state" name="name" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Street: </label>
								<input type="text" id="venue_street" name="name" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Phone: </label>
								<input type="text" id="venue_phone" name="name" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Facebook: </label>
								<input type="text" id="venue_fb" name="name" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Twitter: </label>
								<input type="text" id="venue_twitter" name="name" class="form-control" >
							</div>
						</div>
					</div>
						
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Instagram: </label>
								<input type="text" id="venue_insta" name="name" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Youtube: </label>
								<input type="text" id="venue_utube" name="name" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Website: </label>
								<input type="text" id="venue_web" name="name" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Email: </label>
								<input type="text" id="venue_email" name="name" class="form-control" >
							</div>
						</div>
					</div>

			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveVenue()" class="btn btn-primary" >Save</button>
				<button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->