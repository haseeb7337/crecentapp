<!DOCTYPE html>
<html>
    <head>
         <?php include ('header.php'); ?>
		 
		 <script>
		 var typeid;
		 var genderid;
		 var category;
		 //recpType
		 function getTypes(){
		 var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				for (var i = 0; i < jsonData.result.length; i++) {
					var counter = jsonData.result[i];
					
					var option = document.createElement("option");
					option.text = counter.recp_type_name;
					option.value = counter.recp_type_id;
					if(i == 0){
						typeid = counter.recp_type_id;
					}
					
					select = document.getElementById("recpType");
					select.appendChild(option);
				}
			  }
			});

			xhr.open("GET", "http://api.chariapp.com/public/recp/types");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "3845383e-6098-4e15-a480-27b7a74c2660");
			
			document.getElementById('recpType').onchange = function(){
				document.getElementById('basic-form').action = '/'+this.value;
				typeid = this.value;
				alert(this.value);
			}
			
			document.getElementById('gender').onchange = function(){
				document.getElementById('basic-form').action = '/'+this.value;
				genderid = this.value;
				alert(this.value);
			}
			
			
			document.getElementById('cat').onchange = function(){
				document.getElementById('basic-form').action = '/'+this.value;
				category = this.value;
				alert(this.value);
			}


			xhr.send(data);
		 }
		 
		 function registerRecp(){
			// alert("all done");
			 //var url = 'dashboard.php';
			//window.location.href = url;
			
			alert(genderid);
			
			var refid = document.getElementById('refid').value;
			var refform = document.getElementById('refform').value;
			var fname = document.getElementById('fname').value;
			var lname = document.getElementById('lname').value;
			var dob = document.getElementById('dob').value;
			var nic = document.getElementById('nic').value;
			var mobile = document.getElementById('mobile').value;
			var home = document.getElementById('home').value;
			var email = document.getElementById('email').value;
			var country = document.getElementById('autocomplete-ajax').value;
			var city = document.getElementById('city').value;
			var state = document.getElementById('state').value;
			var pocode	= document.getElementById('pocode').value;
			var st	= document.getElementById('st').value;
			
			//alert(country);
			
			if(!refid){
				 refid = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
			}
			
			/*var data = JSON.stringify({
			  "org_id": "1",
			  "recp_refid": ""+refid+"",
			  "recp_refForms": ""+refform+"",
			  "recp_type_id": ""+typeid+"",
			  "recp_firstName": ""+fname+"",
			  "recp_lastName": ""+lname+"",
			  "recp_dob": ""+dob+"",
			  "recp_gender": ""+genderid+"",
			  "recp_status": "Active",
			  "recp_category": ""+category+"",
			  "recp_country": ""+country+"",
			  "recp_state": ""+state+"",
			  "recp_postcode": ""+pocode+"",
			  "recp_city": ""+city+"",
			  "recp_street": ""+st+"",
			  "recp_area": "",
			  "recp_mobileNo": ""+mobile+"",
			  "recp_homePhone": ""+home+"",
			  "recp_email": ""+email+"",
			  "recp_nationalId": ""+nic+""
			});

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
					alert("Receipent has been Registered");
					var url = 'dashboard.php';
					window.location.href = url;
			  }
			});

			xhr.open("POST", "http://api.chariapp.com/public/recp/create");
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "99f282b2-8d40-4aa6-a248-a29dbceaafa6");

			xhr.send(data);*/
		 }
		 
		 function randomString(length, chars) {
			var result = '';
			for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
			return result;
		}
		 </script>

    </head>


    <body onload="getTypes()">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/logo.png" alt="" >
                            </span>
                            <i>
                                <img src="assets/images/logo_sm.png" alt="" height="28">
                            </i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                        <h5><a href="#">Maxine Kennedy</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
					 <?php include ('menu.php'); ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>


         

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h6 class="text-overflow m-0">Welcome !</h6>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-head"></i> <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-cog"></i> <span>Settings</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-help"></i> <span>Support</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-lock"></i> <span>Lock Screen</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">Receipents Management </h4>
                                   
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">
												<div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Add New User</b></h4>
                                   

                                    <div class="pull-in">
                                        <form id="basic-form" action="javascript:;" onsubmit="registerRecp(this);">
                                            <div>
                                                <h3>Initail Details</h3>
                                                <section>
												
													<div class="form-group clearfix">
                                                        <label class="control-label " for="refid">Receipent Refer Id</label>
                                                        <div class="">
                                                            <input class="form-control required" id="refid" name="refid" type="text" required placeholder="Enter reference id in case of returing user">
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="refform">Refer Form</label>
                                                        <div class="">
                                                            <input class="form-control required" id="refform" name="refform" type="text" required placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="recpType"> Receipent Type</label>
                                                        <div class="">
                                                            <select id="recpType" class="form-control">
                                                               
                                                            </select>

                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>Personal Information</h3>
                                                <section> 
													<div class="form-group clearfix">
                                                        <label class="control-label " for="fname">First Name</label>
                                                        <div class="">
                                                            <input class="form-control required" id="fname" name="fname" type="text" required placeholder="">
                                                        </div>
                                                    </div>
													<div class="form-group clearfix">
                                                        <label class="control-label " for="lname">Last Name</label>
                                                        <div class="">
                                                            <input class="form-control required" id="lname" name="lname" type="text" required placeholder="">
                                                        </div>
                                                    </div>
													<div class="form-group clearfix">
                                                        <label class="control-label " for="dob">Date of Birth</label>
                                                        <div class="">
                                                            <input class="form-control required" id="dob" name="dob" type="date" required placeholder="">
                                                        </div>
                                                    </div>
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="nic">National Identiy Number</label>
                                                        <div class="">
                                                            <input class="form-control required" id="nic" name="nic" type="text" required placeholder="">
                                                        </div>
                                                    </div>
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="refform">Gender</label>
                                                        <div class="">
                                                           <select id="gender" class="form-control">
                                                              <option value="Male">Male</option>
															  <option value="Female">Female</option>
															 
                                                            </select>
															
                                                        </div>
                                                    </div>
													<div class="form-group clearfix">
                                                        <label class="control-label " for="refform">Category</label>
                                                        <div class="">
                                                             <select id="cat" class="form-control">
                                                              <option value="Single">Single</option>
															  <option value="Married">Married</option> 
															  <option value="Divorced">Divorced</option>
															  <option value="Widow">Widow</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>Contact Information</h3>
                                                <section>
                                                   <div class="form-group clearfix">
                                                        <label class="control-label " for="mobile">Mobile #</label>
                                                        <div class="">
                                                            <input class="form-control required" id="mobile" name="mobile" type="number" required placeholder="">
                                                        </div>
                                                    </div>
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="home">Home #</label>
                                                        <div class="">
                                                            <input class="form-control required" id="home" name="home" type="number" required placeholder="">
                                                        </div>
                                                    </div>
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="email">Email Address</label>
                                                        <div class="">
                                                            <input class="form-control required" id="email" name="email" type="email" required placeholder="">
                                                        </div>
                                                    </div>
                                                </section>
												<h3>Address</h3>
                                                <section>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="sdate">Country </label>
                                                        <div class="">
                                                            <input type="text" name="country" id="autocomplete-ajax"
																   class="form-control">
															<input type="text" name="country" id="autocomplete-ajax-x"
																   disabled="disabled" class="form-control"
																   style="color: #CCC; position: absolute; background: transparent; z-index: 1;display: none;"/>
                                                        </div>
                                                    </div>
													
													
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="city">City </label>
                                                        <div class="">
                                                            <input class="form-control" type="text" name="city" id="city">
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="state">State</label>
                                                        <div class="">
                                                             <input class="form-control" type="text" name="state" id="state">
                                                        </div>
                                                    </div>
													
													 <div class="form-group clearfix">
                                                        <label class="control-label " for="pocode">Postal Code</label>
                                                        <div class="">
                                                             <input class="form-control" type="text" name="pocode" id="pocode">
                                                        </div>
                                                    </div>
													
													 <div class="form-group clearfix">
                                                        <label class="control-label " for="st">Street Address</label>
                                                        <div class="">
                                                             <textarea class="form-control" type="text" name="st" id="st"></textarea>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                   <?php include ('footer.php'); ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		
		
		<script type="text/javascript" src="../plugins/autocomplete/jquery.mockjax.js"></script>
        <script type="text/javascript" src="../plugins/autocomplete/jquery.autocomplete.min.js"></script>
        <script type="text/javascript" src="../plugins/autocomplete/countries.js"></script>
        <script type="text/javascript" src="assets/pages/jquery.autocomplete.init.js"></script>

        <!-- Init Js file -->
        <script type="text/javascript" src="assets/pages/jquery.form-advanced.init.js"></script>
		
		<script src="../plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>
		<!--wizard initialization-->
        <script src="assets/pages/jquery.wizard-init.js" type="text/javascript"></script>


        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>