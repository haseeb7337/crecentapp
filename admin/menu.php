<div id="sidebar-menu">

	<ul class="metismenu" id="side-menu">

		<!--<li class="menu-title">Navigation</li>-->

		<li>
			<a href="dashboard.php">
				<i class="fi-air-play"></i><span class="badge badge-danger badge-pill pull-right">7</span> <span class="-blue"> Main</span><span>Dashboard </span>
			</a>
		</li>
		
		<li>
			<a href="javascript: void(0);"><i class="fa fa-user-circle-o"></i><span class="-blue"> User</span><span>Management </span> <span class="menu-arrow"></span></a>
			<ul class="nav-second-level" aria-expanded="false">
				<li><a href="add_user.php">Add New User</a></li>
				<li><a href="userview.php">View All Users</a></li>
			</ul>
		</li>

		<li>
			<a href="viewdoners.php"><i class="fa fa-user-circle-o"></i><span class="-blue"> Doner</span><span>Management </span>  </a>
			 
		</li>
		
		<li>
			<a href="javascript: void(0);"><i class="fa fa-ticket"></i><span class="-blue"> Coupon</span><span>Management </span> <span class="menu-arrow"></span></a>
			<ul class="nav-second-level" aria-expanded="false">
				<li><a href="create_coupons.php">Create Organization</a></li>
				<!-- <li><a href="assing_coupons.php">View Coupons</a></li>  -->
			</ul>
		</li>

		<li>
			<a href="javascript: void(0);"><i class="fa  fa-tasks"></i><span class="-blue"> Charity</span><span>Management </span> <span class="menu-arrow"></span></a>
			<ul class="nav-second-level" aria-expanded="false">
				<li><a href="create_project.php">Create Project</a></li>
				<li><a href="view_projects.php">View Projects</a></li>
				
			</ul>
		</li>
		
		<li>
			<a href="javascript: void(0);"><i class="fa  fa-tasks"></i><span class="-blue"> Receipent</span><span>Management  </span> <span class="menu-arrow"></span></a>
			<ul class="nav-second-level" aria-expanded="false">
				<li><a href="create_receipent.php">Create Receipent</a></li>
				<li><a href="view_receipent.php">View Receipent</a></li>
				<li><a href="view_donors.php">View Assinged Donors</a></li>
				<li><a href="user_requests.php">User Requests</a></li>
				
				
			</ul>
		</li>
		
		
		

	</ul>

</div>