<!DOCTYPE html>
<html>
    <head>
       <?php include ('header.php');?>
 
		
		<script>
	 
            
		function LoadTableData()
		{
            var org_id;
            var queryString = decodeURIComponent(window.location.search);
            queryString = queryString.substring(1);
            var queries = queryString.split("&");
            for (var i = 0; i < queries.length; i++)
            {
                org_id = queries[i];
                var thenum = org_id.replace( /^\D+/g, '');
                // document.write(queries[i] + "<br>");
                console.log(org_id);
            }
                                 
            var data = null;

            var xhr = new XMLHttpRequest();
            xhr.withCredentials = false;

            xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
               // console.log(this.responseText);
                	var jsonData = JSON.parse(this.responseText);
					 //console.log(jsonData.result.userprofile);
					console.log(jsonData.result.recp);
                   
                    var counter = jsonData.result.userprofile[0];

                    var current = document.getElementById("first_name");
                    current.value = counter.user_first_name;

                    var current = document.getElementById("last_name");
                    current.value = counter.user_last_name;

                    var current = document.getElementById("mobile");
                    current.value = counter.user_mobile;

                    var current = document.getElementById("home_phone");
                    current.value = counter.user_home_phone;

                    var current = document.getElementById("doner_email");
                    current.value = counter.user_email;
                    
                    var current = document.getElementById("doner_country");
                    current.value = counter.user_add_cont;

                    var current = document.getElementById("doner_state");
                    current.value = counter.user_add_state;

                    var current = document.getElementById("doner_city");
                    current.value = counter.user_add_city;

                    var current = document.getElementById("doner_address");
                    current.value = counter.user_add_st;

                    var current = document.getElementById("doner_role");
                    current.value = counter.role_name;
                    
                    
                    current = null;

                    //network table
				var t = $('#network_table').DataTable();
				t.clear().draw();
                for (var i = 0; i < jsonData.result.usernetwork.length; i++) {
					//getting each node in array in a seperate varibable
					var networkCounter = jsonData.result.usernetwork[i];
					//adding to the table
					t.row.add([
                        networkCounter.network_firstname,
						
						networkCounter.network_email,
						networkCounter.network_phone
						
					]).draw(true);
				}//for loop ends over here

    

                // Assign Recipient
                var t = $('#receipient_table').DataTable();
				t.clear().draw();
                for (var i = 0; i < jsonData.result.recp.length; i++) {
					//getting each node in array in a seperate varibable
					var recpCounter = jsonData.result.recp[i];
					//adding to the table
					t.row.add([
                        recpCounter.recp_firstName+' '+recpCounter.recp_lastName,
                        recpCounter.recp_gender,
						recpCounter.recp_dob,
						recpCounter.recp_class,
                        recpCounter.recp_city  ,
                        recpCounter.recp_status,
                        "<a href='single_family_info.php?recp_id="+recpCounter.recp_id+"'  class='btn btn-sm btn-custom'>View Details </a>"
						
					]).draw(true);
				}//for loop ends over here


                 // Requests Table
                var t = $('#request_table').DataTable();
				t.clear().draw();
                for (var i = 0; i < jsonData.result.requests.length; i++) {
					//getting each node in array in a seperate varibable
					var requestCounter = jsonData.result.requests[i];
					//adding to the table
					t.row.add([
                        requestCounter.user_first_name,
						requestCounter.user_last_name,
                        requestCounter.user_mobile,
                        requestCounter.amount_donated

						
					]).draw(true);
				}//for loop ends over here


            }
            });

            //    xhr.open("GET", "http://api.chariapp.com/public/users/get/details/1/1");
            xhr.open("GET", "http://api.chariapp.com/public/users/get/details/"+<?php echo $_REQUEST['org_id']; ?>+"/"+<?php echo $_REQUEST['ou_id']; ?>);
            xhr.setRequestHeader("Cache-Control", "no-cache");
            xhr.setRequestHeader("Postman-Token", "1f55705d-9bed-491a-89de-04b9e51c9a0d");

            xhr.send(data);
                        
		}
		
		 
		</script>

    </head>

 
    <body onload="LoadTableData()" >
      
        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/logo.png" alt="" >
                            </span>
                            <i>
                                <img src="assets/images/logo_sm.png" alt="" height="28">
                            </i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                        <h5><a href="#">Maxine Kennedy</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
						<?php include ('menu.php'); ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->


            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>

                            

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h6 class="text-overflow m-0">Welcome !</h6>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-head"></i> <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-cog"></i> <span>Settings</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-help"></i> <span>Support</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-lock"></i> <span>Lock Screen</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">View All Doners </h4>
                                   
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                    <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <h4 class="header-title mb-4">Account Overview</h4>

                                    <div class="row">
                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <input data-plugin="knob" data-width="80" data-height="80" data-linecap=round
                                                           data-fgColor="#0acf97" value="4" data-skin="tron" data-angleOffset="180"
                                                           data-readOnly=true data-thickness=".1"/>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Total Doners</p>
                                                    <h3 class="">4</h3>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <input data-plugin="knob" data-width="80" data-height="80" data-linecap=round
                                                           data-fgColor="#f9bc0b" value="4" data-skin="tron" data-angleOffset="180"
                                                           data-readOnly=true data-thickness=".1"/>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Active Doners</p>
                                                    <h3 class="">4</h3>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <input data-plugin="knob" data-width="80" data-height="80" data-linecap=round
                                                           data-fgColor="#f1556c" value="3" data-skin="tron" data-angleOffset="180"
                                                           data-readOnly=true data-thickness=".1"/>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Charity Payers</p>
                                                    <h3 class="">3  </h3>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <input data-plugin="knob" data-width="80" data-height="80" data-linecap=round
                                                           data-fgColor="#2d7bf4" value="3" data-skin="tron" data-angleOffset="180"
                                                           data-readOnly=true data-thickness=".1"/>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Total Revenue</p>
                                                    <h3 class="">$320</h3>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                        </div>
                    <div class="row">
                    <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Doner Information </h4>

                                    <ul class="nav nav-pills navtab-bg nav-justified pull-in ">
                                        <li class="nav-item">
                                            <a href="#home1" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                               <i class="fi-head mr-2"></i>Profile
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#profile1" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                            <i class="fi-monitor mr-2"></i> My Network
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#messages1" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                <i class="fi-mail mr-2"></i> My Family
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#settings1" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                <i class="fi-cog mr-2"></i> Requests
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane show active" id="home1">
                                            <div class="col-sm-12 card-box"> 
                                            
                                            <div class="modal-body">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> First Name: </label>
                                                                <input type="text" id="first_name" name="first_name" class="form-control" >
                                                            </div>
                                                        </div>	
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Last Name: </label>
                                                                <input type="text" id="last_name" name="last_name" class="form-control" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Mobile : </label>
                                                                <input type="text" id="mobile" name="mobile" class="form-control" >
                                                            </div>
                                                        </div>	
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Home Phone: </label>
                                                                <input type="text" id="home_phone" name="home_phone" class="form-control" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Email: </label>
                                                                <input type="text" id="doner_email" name="doner_email" class="form-control" >
                                                            </div>
                                                        </div>	
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Country: </label>
                                                                <input type="text" id="doner_country" name="doner_country" class="form-control" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>State: </label>
                                                                <input type="text" id="doner_state" name="doner_state" class="form-control" >
                                                            </div>
                                                        </div>	
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>City: </label>
                                                                <input type="text" id="doner_city" name="doner_city" class="form-control" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Street Address: </label>
                                                                <input type="text" id="doner_address" name="doner_address" class="form-control" >
                                                            </div>
                                                        </div>	
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>User Role  </label>
                                                                <input type="text" id="doner_role" disabled name="doner_role" class="form-control" >
                                                            </div>
                                                        </div>
                                                    </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" onclick="saveVenue()" class="btn btn-primary" >Save</button>
                                                <button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane " id="profile1">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="card-box">
                                                        <h4 class="header-title mb-3"></h4>
                                                        <div class="table-responsive">
                                                            <table id="network_table"  class="table table-hover table-centered m-0">
                                                                <thead>
                                                                <tr>
                                                                    <th>Network Name</th>
                                                                    <th>Network Email</th>
                                                                    <th>Network Phone</th>
                                                                </tr>
                                                                </thead>
                                                                
                                                            </table>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="messages1">
                                        <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="card-box">
                                                        <h4 class="header-title mb-3"></h4>
                                                        <div class="table-responsive">
                                                            <table id="receipient_table"  class="table table-hover table-centered m-0">
                                                                <thead>
                                                                <tr>
                                                                    <th>Name </th>
                                                                    <th>Gender</th>
                                                                    <th>Age  </th>
                                                                    <th>Class </th>
                                                                    <th>City</th>
                                                                    <th>Status</th>
                                                                    <th>Action</th>

                                                                </tr>
                                                                </thead>
                                                                
                                                            </table>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="settings1">
                                        <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="card-box">
                                                        <h4 class="header-title mb-3"></h4>
                                                        <div class="table-responsive">
                                                            <table id="request_table"  class="table table-hover table-centered m-0">
                                                                <thead>
                                                                <tr>
                                                                    <th>First Name</th>
                                                                    <th>Last Name </th>
                                                                    <th>Phone </th>
                                                                    <th>Amount Donated</th>
                                                                   

                                                                </tr>
                                                                </thead>
                                                                
                                                            </table>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
            
               
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                   <?php include ('footer.php')?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="../plugins/jquery-knob/jquery.knob.js"></script>
		<!-- Required datatable js -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="../plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		
		<script src="../plugins/custombox/js/custombox.min.js"></script>
        <script src="../plugins/custombox/js/legacy.min.js"></script>

       
		<script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

    </body>
</html>