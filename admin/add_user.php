<!DOCTYPE html>
<html>
    <head>
        <?php include ('header.php'); ?>
		
		<script>
			
		function handleClick(){
				//alert("Submitted");
			var refid = document.getElementById('refid').value;
			var useremail = document.getElementById('useremail').value;
			var password = document.getElementById('password').value;
			var firstname = document.getElementById('firstname').value;
			var lastname = document.getElementById('lastname').value;
			var phone = document.getElementById('phone').value;
			var country = document.getElementById('country').value;
			var city = document.getElementById('city').value;
			var address = document.getElementById('address').value;
			
			var message;
			
			if(!address){
				document.getElementById('address').value = "";
			}
			
			if(!refid){
				document.getElementById('refid').value = "";
			}
			
			if(!useremail ){
				message = "Plese provide user email address";
				//alert("Please enter all information");
			}
			if(!password ){
				message = "Plese provide user password";
				//alert("Please enter all information");
			}
			if(!firstname ){
				message = "Plese provide Firstname";
				//alert("Please enter all information");
			}
			if(!lastname ){
				message = "Plese provide Lastname";
				//alert("Please enter all information");
			}
			if(!phone ){
				message = "Plese provide a Contact Number";
				//alert("Please enter all information");
			}
			
			if(!country ){
				message = "Plese provide Country of Residence";
				//alert("Please enter all information");
			}
			
			if(!city){
				message = message+ "\nPlease list your city";
			}
			
			if(message){
				alert(message);
			}else{
				var data = JSON.stringify({
					  "user_ref_id": ""+refid+"",
					  "registrar_id": "",
					  "user_first_name": ""+firstname+"",
					  "user_last_name": ""+lastname+"",
					  "user_mobile": ""+phone+"",
					  "user_email": ""+useremail+"",
					  "user_add_cont": ""+country+"",
					  "user_add_state": "",
					  "user_add_post_code": "",
					  "user_add_city": ""+city+"",
					  "user_add_st": ""+address+"",
					  "user_password": ""+password+"",
					  "org_id": 1
					});

					var xhr = new XMLHttpRequest();
					xhr.withCredentials = false;

					xhr.addEventListener("readystatechange", function () {
					  if (this.readyState === 4) {
						console.log(this.responseText);
						alert(this.responseText.status);
					  }
					});

					xhr.open("POST", "http://api.chariapp.com/public/users/register");
					xhr.setRequestHeader("Content-Type", "application/json");
					xhr.setRequestHeader("Cache-Control", "no-cache");
					xhr.setRequestHeader("Postman-Token", "471d262f-0943-499c-ad0e-be2677056812");

					xhr.send(data);
			}
			
		}
		
		</script>
		

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/logo.png" alt="" >
                            </span>
                            <i>
                                <img src="assets/images/logo_sm.png" alt="" height="28">
                            </i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                        <h5><a href="#">Maxine Kennedy</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
						<?php include ('menu.php'); ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>

                            

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h6 class="text-overflow m-0">Welcome !</h6>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-head"></i> <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-cog"></i> <span>Settings</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-help"></i> <span>Support</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-lock"></i> <span>Lock Screen</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">Add New User </h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Crecent Connection</a></li>
                                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                                        <li class="breadcrumb-item active">New User</li>
                                    </ol>
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

						<div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Add New User</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Use the button classes on an <code>&lt;a&gt;</code>, <code>&lt;button&gt;</code>, or <code>&lt;input&gt;</code> element.
                                    </p>

                                    <div class="pull-in">
                                        <form id="basic-form" action="javascript:handleClick()">
                                            <div>
                                                <h3>Account</h3>
                                                <section>
												
													<div class="form-group clearfix">
                                                        <label class="control-label " for="refid">User Reference ID </label>
                                                        <div class="">
                                                            <input class="form-control required" id="refid" name="refid" type="text" required placeholder="Reference Id is required in case of returing user">
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="useremail">Email *</label>
                                                        <div class="">
                                                            <input class="form-control required" id="useremail" name="useremail" type="email" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="password"> Password *</label>
                                                        <div class="">
                                                            <input id="password" name="password" type="text" class="required form-control">

                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>Profile</h3>
                                                <section>
                                                    <div class="form-group clearfix">

                                                        <label class="control-label" for="firstname"> First name *</label>
                                                        <div class="">
                                                            <input id="firstname" name="firstname" type="text" class="required form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="lastname"> Last name *</label>
                                                        <div class="">
                                                            <input id="lastname" name="lastname" type="text" class="required form-control">

                                                        </div>
                                                    </div>
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="phone"> Phone Number *</label>
                                                        <div class="">
                                                            <input id="phone" name="phone" type="text" class="required form-control">

                                                        </div>
                                                    </div>

                                                   

                                                </section>
                                                <h3>Address Details</h3>
                                                <section>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="country">Country </label>
                                                        <div class="">
                                                            <input id="country" name="country" type="text" class="required email form-control">
                                                        </div>
                                                    </div>
													
													  <div class="form-group clearfix">
                                                        <label class="control-label " for="city">City </label>
                                                        <div class="">
                                                            <input id="city" name="city" type="text" class="required email form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="address">Street Address </label>
                                                        <div class="">
                                                            <input id="address" name="address" type="text" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="control-label ">(*) Mandatory</label>
                                                    </div>
                                                </section>
                                                
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                   <?php include ('footer.php'); ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		<script src="../plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="assets/pages/jquery.wizard-init.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>