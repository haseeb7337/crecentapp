<!DOCTYPE html>
<html>
    <head>
	 <!-- DataTables -->
        <link href="../plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="../plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <?php include ('header.php'); ?>
		
		
		
		<script>
		function LoadTableData()
		{
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				
				//init the table and clear table for new entires
				var t = $('#vouchertable').DataTable();
				t.clear().draw();
				//for loop to get all the result to table
				for (var i = 0; i < jsonData.result.length; i++) {
					//getting each node in array in a seperate varibable
					var counter = jsonData.result[i];
					
					//adding to the table
					t.row.add([
						counter.org_display_name,
						"<a href='#' onclick='addCoup("+counter.org_id+" , "+counter.org_ref_id+")' class='btn btn-sm btn-custom'><i class='mdi mdi-plus'></i>Create Coupons</a> <a href='#' onclick='viewCoups("+counter.org_id+")' class='btn btn-sm btn-danger'><i class='mdi mdi-minus'></i>View Coupons</a>"
						
					]).draw(true);
				}//for loop ends over here
			  }
			});

			xhr.open("GET", "http://api.chariapp.com/public//benifits/sponserorg/1");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "70ec44e7-56bf-41f5-90cf-1aa8f60641be");

			xhr.send(data);
		}
		
		var referenceId;
		var orgid;
		function closeModol(){
			location.reload();
		}
		
		
		function addCoup(org_id , org_ref_id){
			//alert(org_ref_id);
			orgid = org_id;
			referenceId = org_ref_id
			$('.bs-example-modal-lg-coup').modal('show');
					
		}
		
		function saveCoup(){
			var offer_name = document.getElementById('offer_name').value;
			var startdate = document.getElementById('startdate').value;
			var enddate = document.getElementById('enddate').value;
			var location_city = document.getElementById('location_city').value;
			var discount = document.getElementById('discount').value;
			var coup_des = document.getElementById('coup_des').value;
			
			
			if( startdate < enddate){
			   //alert("start time is lesser");
			   var voucher_code = randomString(7, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
				//alert(voucher_code);
				
			var data = JSON.stringify({
			  "org_id": ""+orgid+"",
			  "offer_name": ""+offer_name+"",
			  "voucher_number": ""+voucher_code+"",
			  "voucher_start_date": ""+startdate+"",
			  "voucher_end_date": ""+enddate+"",
			  "voucher_location": ""+location_city+"",
			  "voucher_conditions": ""+coup_des+"",
			  "voucher_fine_print": "No",
			  "voucher_discount": ""+discount+"",
			  "org_ref_id": ""+referenceId+"",
			});

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				alert("New Coupon has been created..!!");
				closeModol();
			  }
			});

			xhr.open("POST", "http://api.chariapp.com/public/benifits/create");
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "7e67f057-ae8a-499d-916f-b6bdb628601e");

			xhr.send(data);	
			}else{
				alert("Please check your dates again. Start date cannot be later then of end date");
			}
		
			
			
		}
		
		
		function viewCoups(org_id){
			//alert(org_id);
			$('.bs-coups-modal-lg-coup').modal('show');
			
			var t = $('#copuonsViewTable').DataTable();
			t.clear().draw();
			
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				for (var i = 0; i < jsonData.result.length; i++) {
					//getting each node in array in a seperate varibable
					var counter = jsonData.result[i];
					
					//adding to the table
					t.row.add([
						counter.voucher_number,
						counter.offer_name,
						counter.voucher_end_date,
						counter.voucher_location,
						counter.voucher_discount,
						"<a href='#'  class='btn btn-sm btn-custom'><i class='mdi mdi-plus'></i></a> <a href='#' class='btn btn-sm btn-danger'><i class='mdi mdi-minus'></i></a>"
						
					]).draw(true);
				}//for loop ends over here
			  }
			});

			xhr.open("GET", "http://api.chariapp.com/public/benifits/admincoupons/"+org_id);
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "96020c35-fa2b-41bc-af28-c41ec856eafa");

			xhr.send(data);
		}
		
		function randomString(length, chars) {
			var result = '';
			for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
			return result;
		}

		
		
		
		</script>

    </head>


    <body onload="LoadTableData()">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/logo.png" alt="" >
                            </span>
                            <i>
                                <img src="assets/images/logo_sm.png" alt="" height="28">
                            </i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                        <h5><a href="#">Maxine Kennedy</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
                   

						<?php include ('menu.php'); ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>

                          

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h6 class="text-overflow m-0">Welcome !</h6>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-head"></i> <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-cog"></i> <span>Settings</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-help"></i> <span>Support</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-lock"></i> <span>Lock Screen</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">Coupons Management </h4>
                                   
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

							<div class="row">
								<div class="col-lg-12">
									<div class="card-box">
										<h4 class="header-title mb-3">All Coupons</h4>
										<div class="table-responsive">
											<table id="vouchertable"  class="table table-hover table-centered m-0">
												<thead>
												<tr>
													
													<th>Organization Name</th>
													<th>Action</th>
												</tr>
												</thead>
												
											</table>
										</div>
										   <!-- Signup modal content -->
											<?php include('includes/assing_voucher.php'); ?>
											<?php include('includes/add_new_coup.php'); ?>
											<?php include('includes/view_coupons_modol.php'); ?>
											
											 <div class="button-list">
                                        <!-- <button onclick="create_coupon.php" type="button" class="btn btn-info waves-effect waves-light" ></button> -->
										 <a href="create_coupon_org.php"  class="btn btn-info waves-effect waves-light">Add New Organization</a>
											</div>

									</div>
									
								</div>
								 
							</div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                   <?php include ('footer.php'); ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
<!-- Required datatable js -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="../plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		
		<script src="../plugins/custombox/js/custombox.min.js"></script>
        <script src="../plugins/custombox/js/legacy.min.js"></script>
		
		<script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

    </body>
</html>