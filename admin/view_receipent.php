<!DOCTYPE html>
<html>
    <head>
	        <link href="../plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="../plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <?php include ('header.php'); ?>
		
		
		<script>
		
		function LoadProject(){
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				
				//init the table and clear table for new entires
				var t = $('#projtable').DataTable();
				t.clear().draw();
				//for loop to get all the result to table
				for (var i = 0; i < jsonData.result.length; i++) {
					//getting each node in array in a seperate varibable
					var counter = jsonData.result[i];
					
					//adding to the table
					t.row.add([
						counter.recp_firstName+' '+counter.recp_lastName,
						counter.recp_refForms,
						counter.recp_refid,
						counter.recp_gender,
						counter.recp_category,
						"<a href='#' onclick='assingdonor("+counter.recp_id+")' class='btn btn-sm btn-custom'><i class='mdi mdi-plus'></i></a> <a href='#' class='btn btn-sm btn-danger'><i class='mdi mdi-minus'></i></a>"
						
					]).draw(true);
				}//for loop ends over here
			  }
			});

			xhr.open("GET", "http://api.chariapp.com/public/recp/get/1");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "a67c6397-f653-47d1-be17-cf5a5a120057");

			xhr.send(data);
		}
		var status_id;
		var new_recp_id;
		function assingdonor(recp_id){
			//alert(voucher_id);
			new_recp_id = recp_id;
			//
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				for (var i = 0; i < jsonData.result.length; i++) {
					var counter = jsonData.result[i];
					
					var option = document.createElement("option");
					option.text = counter.user_first_name;
					option.value = counter.ou_id;
					if(i == 0){
						status_id = counter.ou_id;
					}
					
					select = document.getElementById("donoruser");
					select.appendChild(option);
				}
				$('.bs-example-modal-lg').modal('show');	
			  }
			});

			xhr.open("GET", "http://api.chariapp.com/public/users/all/1");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "1bad9f21-8fca-468d-a892-936e89e93c3b");
			document.getElementById('donoruser').onchange = function(){
				document.getElementById('form_id').action = '/'+this.value;
				status_id = this.value;
				alert(this.value);
			}

			xhr.send(data);
			
		}
		
		function assingfinal(){
			//alert(new_recp_id +'--'+status_id);
			var data = JSON.stringify({
				  "recp_id": ""+new_recp_id+"",
				  "ou_id": ""+status_id+""
				});

				var xhr = new XMLHttpRequest();
				xhr.withCredentials = false;

				xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
					console.log(this.responseText);
					alert("A donor has been assinged to Recepient");
					location.reload();
				  }
				});

				xhr.open("POST", "http://api.chariapp.com/public/recp/assing");
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.setRequestHeader("Cache-Control", "no-cache");
				xhr.setRequestHeader("Postman-Token", "b81fbcce-c4c3-4286-8cca-75b81d95694e");

				xhr.send(data);
		}
		
		function closeModol(){
			location.reload();
		}
		</script>

    </head>


    <body onload="LoadProject()">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/logo.png" alt="" >
                            </span>
                            <i>
                                <img src="assets/images/logo_sm.png" alt="" height="28">
                            </i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                        <h5><a href="#">Maxine Kennedy</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
						<?php include ('menu.php'); ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>

                           

                            

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h6 class="text-overflow m-0">Welcome !</h6>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-head"></i> <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-cog"></i> <span>Settings</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-help"></i> <span>Support</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-lock"></i> <span>Lock Screen</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">Project Viewer </h4>
                                   
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

							<div class="row">
								<div class="col-lg-12">
									<div class="card-box">
										<h4 class="header-title mb-3">Project Details</h4>
										<div class="table-responsive">
											<table id="projtable"  class="table table-hover table-centered m-0">
												<thead>
												<tr>
													<th>Name</th>
													<th>Program Name</th>
													<th>Reference Id</th>
													<th>Gender</th>
													<th>Category</th>
													<th>Action</th>
												</tr>
												</thead>
												
											</table>
										</div>
										   <!-- Signup modal content -->
										   <?php include('includes/assing_recp_donor.php'); ?>
									
									</div>
									
								</div>
								 
							</div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                   <?php include ('footer.php'); ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
<!-- Required datatable js -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="../plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		
		<script src="../plugins/custombox/js/custombox.min.js"></script>
        <script src="../plugins/custombox/js/legacy.min.js"></script>
		
		<script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

    </body>
</html>