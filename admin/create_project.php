<!DOCTYPE html>
<html>
    <head>
          <?php include ('header.php'); ?>
		  
		  <script>
		  var current_type_id;
		  var current_status_id;
		   function LoadStarts(){
			   var data = null;

				var xhr = new XMLHttpRequest();
				xhr.withCredentials = false;

				xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
					
					
					var jsonData = JSON.parse(this.responseText);
					console.log(jsonData.result.status);
					console.log(jsonData.result.types);
					console.log(jsonData.result.types.length);
					//var counter = jsonData.result.types[1];
					//console.log(counter.project_type_name);
					//for loading the types
					for (var i = 0; i < jsonData.result.types.length; i++) {
						var counter = jsonData.result.types[i];
						
						var option = document.createElement("option");
						option.text = counter.project_type_name;
						option.value = counter.project_type_id;
						if(i == 0){		
							current_type_id = counter.project_type_id; 							//CORRECT THIS
						}
						
						select = document.getElementById("projectType");
						select.appendChild(option);
					}
					
					//for loading the status
					for (var i = 0; i < jsonData.result.status.length; i++) {
						var counter = jsonData.result.status[i];
						
						var option = document.createElement("option");
						option.text = counter.status_name;
						option.value = counter.status_id;
						if(i == 0){
							current_status_id = counter.status_id;								//CORRECT THIS
						}
						
						select = document.getElementById("projectStatus");
						select.appendChild(option);
					}
				  }
				});

				xhr.open("GET", "http://api.chariapp.com/public/projects/statuslist");
				xhr.setRequestHeader("Cache-Control", "no-cache");
				xhr.setRequestHeader("Postman-Token", "e807de6f-001a-48cb-82d7-2fb64f604a12");
				
				document.getElementById('projectType').onchange = function(){
					document.getElementById('basic-form').action = '/'+this.value;
					current_type_id = this.value;
					alert(this.value);
				}
				
				document.getElementById('projectStatus').onchange = function(){
					document.getElementById('basic-form').action = '/'+this.value;
					current_status_id = this.value;
					alert(this.value);
				}

				xhr.send(data);
		   }
		   
		   function validateFormOnSubmit(){
			   //alert("yooo");
				var projectname = document.getElementById('projectname').value;
				var breif = document.getElementById('breif').value;
				var desc = document.getElementById('desc').value;
				var sdate = document.getElementById('sdate').value;
				var edate = document.getElementById('edate').value;
				
				if( sdate < edate){
				   
				   var data = JSON.stringify({
					  "org_id": "1",
					  "project_type_id": ""+current_type_id+"",
					  "project_name": ""+projectname+"",
					  "project_breif": ""+breif+"",
					  "project_details": ""+desc+"",
					  "project_logo": "",
					  "status_id": ""+current_status_id+"",
					  "project_start_date": ""+sdate+"",
					  "project_end_date": ""+edate+""
					});

					var xhr = new XMLHttpRequest();
					xhr.withCredentials = false;

					xhr.addEventListener("readystatechange", function () {
					  if (this.readyState === 4) {
						//console.log(this.responseText);
						alert("New Project has been Added");
						location.reload();
					  }
					});

					xhr.open("POST", "http://api.chariapp.com/public/projects/create");
					xhr.setRequestHeader("Content-Type", "application/json");
					xhr.setRequestHeader("Cache-Control", "no-cache");
					xhr.setRequestHeader("Postman-Token", "c04cfd8f-d377-428b-bdd0-93f663229aee");

					xhr.send(data);
				}
				
			   //alert(current_status_id+'---'+current_type_id);

		   }
		  
		  </script>
    </head>


    <body onload="LoadStarts()">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/logo.png" alt="" >
                            </span>
                            <i>
                                <img src="assets/images/logo_sm.png" alt="" height="28">
                            </i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                        <h5><a href="#">Maxine Kennedy</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
					  <?php include ('menu.php'); ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h6 class="text-overflow m-0">Welcome !</h6>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-head"></i> <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-cog"></i> <span>Settings</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-help"></i> <span>Support</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-lock"></i> <span>Lock Screen</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">Add Project </h4>
                                   
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

						<div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Add New User</b></h4>
                                   

                                    <div class="pull-in">
                                        <form id="basic-form" action="javascript:;" onsubmit="validateFormOnSubmit(this);">
                                            <div>
                                                <h3>Initail Details</h3>
                                                <section>
												
													<div class="form-group clearfix">
                                                        <label class="control-label " for="projectname">Project Name</label>
                                                        <div class="">
                                                            <input class="form-control required" id="projectname" name="projectname" type="text" required placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="protype">Project Type</label>
                                                        <div class="">
                                                            <select id="projectType" class="form-control">
                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="breif"> Project Brief </label>
                                                        <div class="">
                                                            <textarea id="breif" name="breif" type="text" class="required form-control"></textarea>

                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>Project Description</h3>
                                                <section> 
													<div class="form-group clearfix">
                                                        <label class="control-label " for="desc"> Project Description </label>
                                                        <div class="">
                                                            <textarea id="desc" name="desc" type="text" class="required form-control"></textarea>

                                                        </div>
                                                    </div>
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="breif"> Project Logo </label>
                                                        <div class="">
                                                           <input type="file" class="form-control">
                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>Project Dates and Status</h3>
                                                <section>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="sdate">Start Date </label>
                                                        <div class="">
                                                            <input class="form-control" type="date" name="sdate" id="sdate">
                                                        </div>
                                                    </div>
													
													<div class="form-group clearfix">
                                                        <label class="control-label " for="edate">End Date </label>
                                                        <div class="">
                                                            <input class="form-control" type="date" name="edate" id="edate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="address">Initail Status </label>
                                                        <div class="">
                                                            <select id="projectStatus" class="form-control">
                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                  <?php include ('footer.php'); ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		<script src="../plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>
		<!--wizard initialization-->
        <script src="assets/pages/jquery.wizard-init.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>