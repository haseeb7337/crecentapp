<?php

//start session
session_start();

//load dependies
require __DIR__ . '/../vendor/autoload.php';



//inti the slim framework
$app = new \Slim\App([

    'settings' =>[
        'displayErrorDetails' => true,
    ]
]);


//setting up the containers for the views any of those are used in here
$container = $app->getContainer();
$container['view'] = function($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views' , [
        'cache' => false,
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));
    return $view;
};

//calling the home controller class
$container['HomeController'] = function($container){
    return new \App\Controllers\HomeController($container);
};




//the main user controller for the app
$container['UserController'] = function($container){
    return new \App\Controllers\UserController($container);
};

//the main OrganizationController controller for the app
$container['OrganizationController'] = function($container){
    return new \App\Controllers\OrganizationController($container);
};


//the feedback controller for the app
$container['FeedbackController'] = function($container){
    return new \App\Controllers\FeedbackController($container);
};


//thefUserNetworkControllers for the app
$container['UserNetworkController'] = function($container){
    return new \App\Controllers\UserNetworkController($container);
};


//ProjectsController for the app
$container['ProjectsController'] = function($container){
    return new \App\Controllers\ProjectsController($container);
};

//RecipientController for the app
$container['RecipientController'] = function($container){
    return new \App\Controllers\RecipientController($container);
};

//RecipientController for the app
$container['BenifitsController'] = function($container){
    return new \App\Controllers\BenifitsController($container);
};


//FamilyController for the app
$container['FamilyController'] = function($container){
    return new \App\Controllers\FamilyController($container);
};




//adding the routues file in here.. 
require __DIR__ . '/../app/routes.php';

?>