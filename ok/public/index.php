<?php

header('Content-type: application/json; charset=utf-8');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, cache-control, postman-token");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
require __DIR__  . '/../bootstrap/app.php';

$app->run();

?>