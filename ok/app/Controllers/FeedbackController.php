<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;

class FeedbackController{

    //general test function
    public function CheckFeedbackTest(){
        echo "Feedback OK....!!";
    }

    //add new feedback
    public function NewUserFeedback(){

    }

    //get all feedbacks for admin side
    public function GetAllFeedback(){
        
    }

    //get feedback by Id
    public function GetFeedbackById(){
        
    }

    // here we can update the feedback date and is feed read options for the user
    public function UpdateFeedbackReadOptions(){
        
    }

    
}

?>