<?php
namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;



class BenifitsController{


    //create voucher
    public function CreateNewVoucher($request, $response){

		$data = $request->getParsedBody();
		
        $org_id = $data['org_id'];
        $offer_name = $data['offer_name'];
        $voucher_number = $data['voucher_number'];
        $voucher_start_date = $data['voucher_start_date'];
        $voucher_end_date = $data['voucher_end_date'];
        $voucher_location = $data['voucher_location'];
        $voucher_conditions = $data['voucher_conditions'];
        $voucher_fine_print = $data['voucher_fine_print'];
        $voucher_discount = $data['voucher_discount'];
        $org_ref_id = $data['org_ref_id'];

        $db = getDB();

        $insertStatement = $db->insert(array('org_id', 'org_ref_id',  'offer_name', 'voucher_number' , 'voucher_start_date', 'voucher_end_date',
			'voucher_location' , 'voucher_conditions' , 'voucher_fine_print' , 'voucher_discount'))
			->into('ca_org_vouchers')
			->values(array($org_id, $org_ref_id ,$offer_name, $voucher_number , $voucher_start_date, $voucher_end_date, $voucher_location , $voucher_conditions , $voucher_fine_print , $voucher_discount));
			
			$insertId = $insertStatement->execute(false);
            $id = $db->lastInsertId();
            if($insertId){
                $data = array('creation' => 'Success', 'msg' => 'New Voucher has been Added.', 'status' => 201 , 'id' => $id);
                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
            }else{
                $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            } 
    }

    // list all vouchers for admin on org
    public function ListAllVouchers($request , $response){
        $db = getDB();

        $orgid = $request->getAttribute('orgid');
    
        $getProjects = "SELECT
                            ca_org_vouchers.voucher_id,
                            ca_org_vouchers.offer_name,
                            ca_org_vouchers.voucher_number,
                            ca_org_vouchers.voucher_start_date,
                            ca_org_vouchers.voucher_end_date,
                            ca_org_vouchers.voucher_location,
                            ca_org_vouchers.voucher_conditions,
                            ca_org_vouchers.voucher_fine_print,
                            ca_org_vouchers.voucher_discount
                            FROM
                            ca_org_vouchers
                            WHERE
                            ca_org_vouchers.org_id = :orgid";
        try
		{
			$stmt = $db->prepare($getProjects);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //get list of assinged vouchers for admin
    public function GetAssingVoucherAdmin($request , $response){
        $db = getDB();

        $orgid = $request->getAttribute('orgid');
    
        $getProjects = "SELECT
                        ca_users.user_first_name,
                        ca_users.user_last_name,
                        ca_org_users_voucher.voucher_redeem_date,
                        ca_org_users_voucher.is_voucher_active,
                        ca_org_vouchers.offer_name,
                        ca_org_vouchers.voucher_number,
                        ca_org_vouchers.voucher_start_date,
                        ca_org_vouchers.voucher_end_date,
                        ca_org_vouchers.voucher_location,
                        ca_org_vouchers.voucher_fine_print,
                        ca_org_vouchers.voucher_conditions,
                        ca_org_vouchers.voucher_discount
                        FROM
                        ca_org_users_voucher
                        INNER JOIN ca_org_users ON ca_org_users_voucher.ou_id = ca_org_users.ou_id
                        INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
                        INNER JOIN ca_org_vouchers ON ca_org_users_voucher.voucher_id = ca_org_vouchers.voucher_id
                        WHERE
                        ca_org_vouchers.org_id = :orgid";
        try
		{
			$stmt = $db->prepare($getProjects);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //assing vouher to user
    public function AssingVoucher($request , $response){
        $data = $request->getParsedBody();
		
        $ou_id = $data['ou_id'];
        $voucher_id = $data['voucher_id'];

        $db = getDB();


        //checking if vouhcer is not already assinged to user
        $selectStatement = $db->select(array('user_voucher_id'))->from('ca_org_users_voucher')
        ->whereMany(array('ou_id' => $ou_id, 'voucher_id' => $voucher_id), '=');
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		$size = sizeof($data);

		if($size > 0)
		{
			$data = array('registration' => 'Failed', 'msg' => 'The Voucher is already assinged to this user', 'status' => 409);
			return $response->withStatus(409)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
		}
        else
        {
            $insertStatement = $db->insert(array('ou_id', 'voucher_id'))
			->into('ca_org_users_voucher')
			->values(array($ou_id, $voucher_id));
			
			$insertId = $insertStatement->execute(false);
            $id = $db->lastInsertId();
            if($insertId){
                $data = array('creation' => 'Success', 'msg' => 'Voucher has been assinged to user', 'status' => 201 , 'id' => $id);
                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
            }else{
                $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            } 
        }

    }

    //get vouhcers for user
    public function GetUserVoucher($request , $response){
        $db = getDB();
        $ouid = $request->getAttribute('ouid');
        $isvoucheractive = $request->getAttribute('isvoucheractive');
        $orgid = $request->getAttribute('orgid');
        
        
        $getProjects = "SELECT
                        ca_org_vouchers.offer_name,
                        ca_org_vouchers.voucher_number,
                        ca_org_vouchers.voucher_start_date,
                        ca_org_vouchers.voucher_location,
                        ca_org_vouchers.voucher_end_date,
                        ca_org_vouchers.voucher_conditions,
                        ca_org_vouchers.voucher_fine_print,
                        ca_org_vouchers.voucher_discount,
                        ca_org_users_voucher.is_voucher_active,
                        ca_org_vouchers.voucher_id
                        FROM
                        ca_org_users_voucher
                        INNER JOIN ca_org_vouchers ON ca_org_users_voucher.voucher_id = ca_org_vouchers.voucher_id
                        WHERE
                        ca_org_users_voucher.ou_id = :ouid AND
                        ca_org_users_voucher.is_voucher_active = :isvoucheractive AND
                        ca_org_vouchers.org_id = :orgid";
        try
		{
			$stmt = $db->prepare($getProjects);
            $stmt->bindParam("ouid", $ouid);
            $stmt->bindParam("isvoucheractive", $isvoucheractive);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //update vouhcer when user used it , this is will also verify the voucher first when scanned
    public function UpdateVouhcer($request , $response){
       
        $db = getDB();
        $data = $request->getParsedBody();

        //$org_id = $data['org_id'];
        $ou_id = $data['ou_id'];
        $voucher_id = $data['voucher_id'];
      //  $redeem_date = $data['redeem_date'];

        //check if voucher exists
        $getvouch = "SELECT
            ca_org_vouchers.voucher_number
            FROM
            ca_org_vouchers
            WHERE ca_org_vouchers.voucher_id = :voucher_id AND ca_org_vouchers.voucher_end_date > DATE(NOW())";   
        try
		{
			$stmt = $db->prepare($getvouch);
            $stmt->bindParam("voucher_id", $voucher_id);
			$stmt->execute();
			$projects = $stmt->fetchAll();
            
            
			$max = sizeof($projects);
			if($max > 0){
                    //insert users
                $date = date('Y-m-d');
                $insertStatement = $db->insert(array('ou_id', 'voucher_id', 'voucher_redeem_date'))
                ->into('ca_org_users_voucher')->values(array($ou_id, $voucher_id, $date));
        
                $insertId = $insertStatement->execute(false);
                if($insertId){
                    $data = array('access' => 'success', 'msg' => 'Voucher has been redeemed Successfully', 'status' => 201);
                    return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
                }else{
                    $data = array('access' => 'forbidden', 'msg' => 'Voucher does not exists', 'status' => 400);
                    return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
                } 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'Voucher does not exists', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
       
       
       
       
     
       




    }

    //while creating the coupons we need to have the list of users
    //that could be assing to an organzations
    public function GetListUsersToAssing($request , $response){
        $db = getDB();
        $orgid = $request->getAttribute('orgid');
        $roleid = $request->getAttribute('roleid');
    
            $getProjects = "SELECT
                            ca_org_users.ou_id,
                            ca_org_users.user_id,
                            ca_users.user_first_name,
                            ca_users.user_last_name
                            FROM
                            ca_org_users
                            INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
                            INNER JOIN ca_org_user_role ON ca_org_user_role.ou_id = ca_org_users.ou_id
                            WHERE
                            ca_org_users.org_id = :orgid AND
                            ca_org_user_role.role_org_id <> :roleid";
        try
		{
			$stmt = $db->prepare($getProjects);
            $stmt->bindParam("orgid", $orgid);
            $stmt->bindParam("roleid", $roleid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }


    //create the organzation for the coupons use only....!!!
    public function CreateCouponOrg($request , $response)
    {
        $data = $request->getParsedBody();
        $db = getDB();
        $orgid = $data['orgid'];
        $ouid = $data['ouid'];
        $role_id = $data['role_id'];
        $org_reg_no = $data['org_reg_no'];
        $org_bus_no = $data['org_bus_no'];
        $org_name = $data['org_name'];
        $org_dis_name = $data['org_dis_name'];
        $org_email = $data['org_email'];
        $org_phone = $data['org_phone'];
        $org_coun = $data['org_coun'];
        $org_state = $data['org_state'];
        $org_city = $data['org_city'];

       

        $insertStatement = $db->insert(array('org_reg_no', 'org_business_no', 'org_business_name' , 'org_display_name', 'org_email',
        'org_main_phone' , 'org_add_count' , 'org_add_state' , 'org_add_city' , 'org_ref_id'))
        ->into('ca_org')->values(array($org_reg_no, $org_bus_no, $org_name , $org_dis_name, $org_email, $org_phone ,
         $org_coun , $org_state , $org_city , $orgid));



         $updateStatement = $db->update(array('role_org_id' => $role_id))
         ->table('ca_org_user_role')
         ->where('ou_id', '=', $ouid);

    
       
        $insertId = $insertStatement->execute(false);
        $id = $db->lastInsertId();
        if($insertId){

            $affectedRows = $updateStatement->execute();
            if($affectedRows){
                $db = null;
                $data = array('creation' => 'Success', 'msg' => 'Organization has been created', 'status' => 201 , 'id' => $id);
                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
            }
            else{
                $data = array('creation' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }

           
        }else{
            $data = array('creation' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        } 
    }


    //get the list of sponser organzation
    public function GetSponserOrgs($request , $response){
        $db = getDB();
        $orgid = $request->getAttribute('orgid');
        $getProjects = "SELECT
                        ca_org.org_id,
                        ca_org.org_display_name,
                        ca_org.org_ref_id
                        FROM
                        ca_org
                        WHERE
                        ca_org.org_ref_id = :orgid";
        try
		{
			$stmt = $db->prepare($getProjects);
            $stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			if($max > 0){
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //get list of vouchers based on organization. this is only for admin part
    public function GetOrgVouchersAdim($request , $response)
    {
        $db = getDB();
        $orgid = $request->getAttribute('orgid');
        $getProjects = "SELECT
                        ca_org_vouchers.voucher_id,
                        ca_org_vouchers.org_id,
                        ca_org_vouchers.org_ref_id,
                        ca_org_vouchers.offer_name,
                        ca_org_vouchers.voucher_number,
                        ca_org_vouchers.voucher_start_date,
                        ca_org_vouchers.voucher_end_date,
                        ca_org_vouchers.voucher_location,
                        ca_org_vouchers.voucher_conditions,
                        ca_org_vouchers.voucher_fine_print,
                        ca_org_vouchers.voucher_discount
                        FROM
                        ca_org_vouchers
                        WHERE
                        ca_org_vouchers.org_id = :orgid";
        try
		{
			$stmt = $db->prepare($getProjects);
            $stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			if($max > 0){
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }


    //get list of vouchers for the users
    public function GetUserBenifits($request , $response)
    {
        $db = getDB();
        $locationName = $request->getAttribute('locationName');
        $orgid = $request->getAttribute('orgid');
        $getProjects = "SELECT
        ca_org_vouchers.voucher_id,
        ca_org_vouchers.org_id,
        ca_org_vouchers.org_ref_id,
        ca_org_vouchers.offer_name,
        ca_org_vouchers.voucher_number,
        ca_org_vouchers.voucher_start_date,
        ca_org_vouchers.voucher_end_date,
        ca_org_vouchers.voucher_location,
        ca_org_vouchers.voucher_conditions,
        ca_org_vouchers.voucher_fine_print,
        ca_org_vouchers.voucher_discount
        FROM
        ca_org_vouchers WHERE ca_org_vouchers.voucher_location = :locationName AND ca_org_vouchers.org_ref_id = :orgid
        AND ca_org_vouchers.voucher_end_date > DATE(NOW())";
        try
		{
			$stmt = $db->prepare($getProjects);
            $stmt->bindParam("locationName", $locationName);
            $stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			if($max > 0){
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }


}

?>