<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;


class WallpaperController
{
    //getting list of all the wallpapers
    public function GetAllWallpapers($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            //getting the parameters 
            $categoryId = $request->getAttribute('catid'); 
            $status = $request->getAttribute('status');

            //get the db 
            $db = getDB();
            //the query
            $selectStatement = $db->select(array('walppr_id', 'image_path' , 'cat_name' , 'fullname'))->from('tbl_wallpapers')
            ->join('tbl_category' , 'tbl_wallpapers.cat_id', '=' , 'tbl_category.cat_id' , 'INNER')
            ->join('tbl_users' , 'tbl_wallpapers.user_id', '=' , 'tbl_users.user_id' , 'INNER')
            ->whereMany(array('tbl_wallpapers.cat_id' => $categoryId, 'is_image_approv' => $status), '=');;

            $stmt = $selectStatement->execute();
            $data = $stmt->fetchAll();

           // print_r($data);
            $size = sizeof($data);
            if($size > 0)
            {
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
                ->write('{"result":'.json_encode($data).'}'); 
            }
            else
            {
                $data = array('msg' => 'No Wallpapers found', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }

        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }


    //adding a new wallpaper
    public function AddWallpaper($request, $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            //getting the request
            $data = $request->getParsedBody();
            $catid = $data['catid'];
            $userid = $data['userid'];

            //get the parameters form the request
            
            //getting the uploaded file details.
            if (!isset($_FILES['uploads'])) 
            { 
                //file not chosed
                $data = array('upload' => 'Failed', 'msg' => 'Please chose a picture to upload', 'status' => 415);
                return $response->withStatus(415)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
            }
            else
            {
                //file was selected and recevied
                
                //check the file type
                $type = $_FILES['uploads']['type'];
                if($type == "image/jpeg" || $type == "image/png")
                {
                    
                    //getting the upload directory
                    $uploaddir =  __DIR__ . '/uploads/';
                    //getting the directory to file to transfer
                    $uploadfile = $uploaddir . basename($_FILES['uploads']['name']);
                    //getting the image name 
                    $userImageName = basename($_FILES['uploads']['name']);

                    //saving the file on the server
                    if (move_uploaded_file($_FILES['uploads']['tmp_name'], $uploadfile)) 
                    {
                        //file saved successfully now save the details to the database 
                        //get the db 
                        $db = getDB();

                        //insert query
                        $insertStatement = $db->insert(array('cat_id', 'user_id', 'image_path'))
                            ->into('tbl_wallpapers')
                            ->values(array($catid, $userid, $userImageName));
                        
                        //executing the statement
                        $insertId = $insertStatement->execute(false);
                        $db = null;
                        if($insertId){
                            $data = array('upload' => 'Success', 'msg' => 'Your Wallpaper has been uploaded successfully.', 'status' => 201);
                            return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));   
                        }else{
                            $data = array('upload' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                        }
                    }
                    //file not saved to server
                    else
                    {
                        $data = array('upload' => 'Failed', 'msg' => 'Unable to upload your picture. Please try again', 'status' => 415);
                        return $response->withStatus(415)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
                    }
                }
                else
                {
                    //wrong file type selected
                    $data = array('upload' => 'Failed', 'msg' => 'You have chosen a wrong File. We support JPG/JPEG and PNG only.', 'status' => 400);
                    return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
                }
            }
        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //delete AddWallpaper
    public function DeleteWallpaper($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {

        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //approve disapprove image
    public function ApproveDisWallpaper($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {

        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //change cateogry for wallpaper 
    public function ChangeCatWallpaper($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {

        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }
}

?>