<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;


class CatController
{

    //create new cateogry
    public function CreateCate($request, $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            //getting the request body
            $data = $request->getParsedBody();

            $userid = $data['userid'];
            $catename = $data['catname'];

            $db = getDB();
            //insert statment to create category
            $insertStatement = $db->insert(array('user_id', 'cat_name'))
            ->into('tbl_category')
            ->values(array($userid, $catename));
            
            //executing the statement
            $insertId = $insertStatement->execute(false);
            $db = null;
            if($insertId){
                $data = array('created' => 'Success', 'msg' => 'New Category has been created.', 'status' => 201);
                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));   
            }else{
                $data = array('created' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //delete cateogry
    public function DeleteCat($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            //getting connection to database
            $db = getDB();

            //get the parameters
            $categoryId = $request->getAttribute('catid'); 

            //delete query
            $deleteStatement = $db->delete()->from('tbl_category')->where('cat_id', '=', $categoryId);
            $affectedRows = $deleteStatement->execute();

            if($affectedRows){
                $data = array('msg' => 'Category has been deleted', 'status' => 200);
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }else{
                $data = array('msg' => 'There was an error deleting the category', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }
        }
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
        
    }

    //get approved & unapproved
    public function GetApprovedCat($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {

            //getting the status
            $getStatus = $request->getAttribute('status');

            //init database
            $db = getDB();

            //the query
            $selectStatement = $db->select(array('cat_id', 'fullname', 'cat_name', 'date_created'))
            ->from('tbl_category')->join('tbl_users', 'tbl_category.user_id', '=', 'tbl_users.user_id', 'INNER')
            ->where('is_approved', '=', $getStatus);

            $stmt = $selectStatement->execute();
            $data = $stmt->fetchAll();

            $size = sizeof($data);
            if($size > 0)
            {
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
                ->write('{"result":'.json_encode($data).'}'); 
            }
            else
            {
                $data = array('msg' => 'No categories found', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }

        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //approve disapprove category
    public function ApproveDisCat($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            //getting the parameters
            $catId = $request->getAttribute('catid');
            $status = $request->getAttribute('status');

            //init db
            $db = getDB();

            //update query
            $updateStatement = $db->update(array('is_approved' => $status))
                       ->table('tbl_category')
                       ->where('cat_id', '=', $catId);

            $affectedRows = $updateStatement->execute();
            if($affectedRows){
                $data = array('msg' => 'Category has been updated', 'status' => 200);
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }else{
                $data = array('msg' => 'There was an error updating the category', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }
        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    

}

?>