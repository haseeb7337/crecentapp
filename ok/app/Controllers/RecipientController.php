<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;


class RecipientController{
    
    //testing function
    public function TestRecp(){
        echo json_encode("Hello World Recp");
    }

    //create the new receipetn the add the recp id and org id to Project recpt table
    //on success creation of recipet. that table will be used to fetch the recp for specifed org

    // UPDATED NOTE. ONLY ADD ORG COLUM IN THE TABLE TO GET ORG RECP
    public function CreateNewRecp($request , $response){
        $data = $request->getParsedBody();
		
        $org_id = $data['org_id'];
        $recp_refid  = $data['recp_refid'];
        $recp_refForms = $data['recp_refForms'];
        $recp_type_id = $data['recp_type_id'];
        $recp_firstName = $data['recp_firstName'];
        $recp_lastName = $data['recp_lastName'];
        $recp_dob = $data['recp_dob'];
        $recp_gender = $data['recp_gender'];
        $recp_status = $data['recp_status'];
        $recp_category = $data['recp_category'];
        $recp_country = $data['recp_country'];
        $recp_state = $data['recp_state'];
        $recp_postcode = $data['recp_postcode'];
        $recp_city = $data['recp_city'];
        $recp_street = $data['recp_street'];
        $recp_area = $data['recp_area'];
        $recp_mobileNo = $data['recp_mobileNo'];
        $recp_homePhone = $data['recp_homePhone'];
        $recp_email = $data['recp_email'];
        $recp_nationalId = $data['recp_nationalId'];


        $db = getDB();

        $insertStatement = $db->insert(array('recp_refid', 'recp_refForms' , 'recp_type_id', 'recp_firstName',
            'recp_lastName' , 'recp_dob' , 'recp_gender' , 'recp_status', 'recp_category' , 'recp_country' , 'recp_state' , 'recp_postcode',
            'recp_city' , 'recp_street' , 'recp_area' , 'recp_mobileNo', 'recp_homePhone' , 'recp_email' , 'recp_nationalId'))
			->into('ca_recipient')
            ->values(array($recp_refid, $recp_refForms , $recp_type_id, $recp_firstName, $recp_lastName , 
            $recp_dob , $recp_gender , $recp_status, $recp_category , $recp_country , $recp_state,
            $recp_postcode , $recp_city , $recp_street, $recp_area , $recp_mobileNo , $recp_homePhone, $recp_email , $recp_nationalId));

            $insertId = $insertStatement->execute(false);
            $recp_id = $db->lastInsertId();
            $project_id = '1'; 
            if($insertId){
                    //creating the project recipet
                    $createProjectRecp = $db->insert(array('org_id', 'project_id', 'recp_id'))
                        ->into('ca_project_recp')
                        ->values(array($org_id, $project_id, $recp_id));
                    $finaRep = $createProjectRecp->execute(false);
                    if($finaRep){
                        $data = array('creation' => 'Success', 'msg' => 'New Receipent has been Added.', 'status' => 201 , 'id' => $id);
                        return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
                    }else{
                        $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                        return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                    }
                
            }
            
           
			

    }

    //get all recp for the organization  for the ref table
    //projectRecipient by org id  

    //AND ca_recp_donor.recp_id is null
    public function GetAllRecpByOrgID($request , $response){
        $db = getDB();
        $orgid = $request->getAttribute('orgid');
        $getProjects = "SELECT
        ca_recipient.recp_id,
        ca_recipient.org_id,
        ca_recipient.recp_refid,
        ca_recipient.recp_refForms,
        ca_recipient.recp_type_id,
        ca_recipient.recp_firstName,
        ca_recipient.recp_lastName,
        ca_recipient.recp_dob,
        ca_recipient.recp_gender,
        ca_recipient.recp_class,
        ca_recipient.recp_status,
        ca_recipient.recp_category,
        ca_recipient.recp_country,
        ca_recipient.recp_state,
        ca_recipient.recp_postcode,
        ca_recipient.recp_city,
        ca_recipient.recp_street,
        ca_recipient.recp_area,
        ca_recipient.recp_mobileNo,
        ca_recipient.recp_homePhone,
        ca_recipient.recp_email,
        ca_recipient.recp_nationalId,
        ca_recp_types.recp_type_name
        FROM
        ca_recipient
        INNER JOIN ca_recp_types ON ca_recipient.recp_type_id = ca_recp_types.recp_type_id
        LEFT JOIN ca_recp_donor ON ca_recp_donor.recp_id = ca_recipient.recp_id
        WHERE
        ca_recipient.org_id = :orgid";
        try
		{
			$stmt = $db->prepare($getProjects);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Recipents Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    
    }

    //get the list of recipients who have been assinged to a donor
    public function GetAllAssingedRec($request , $response)
    {
        $db = getDB();
        $orgid = $request->getAttribute('orgid');

        $getProjects = "SELECT
                        ca_recipient.recp_id,
                        ca_recipient.recp_refid,
                        ca_recp_types.recp_type_name,
                        ca_recipient.recp_refForms,
                        ca_recipient.recp_firstName,
                        ca_recipient.recp_lastName,
                        ca_recipient.recp_dob,
                        ca_recipient.recp_gender,
                        ca_recipient.recp_class,
                        ca_recipient.recp_status,
                        ca_recipient.recp_category,
                        ca_recipient.recp_country,
                        ca_recipient.recp_state,
                        ca_recipient.recp_postcode,
                        ca_recipient.recp_city,
                        ca_recipient.recp_street,
                        ca_recipient.recp_area,
                        ca_recipient.recp_mobileNo,
                        ca_recipient.recp_homePhone,
                        ca_recipient.recp_email,
                        ca_recipient.recp_nationalId,
                        ca_users.user_id,
                        ca_users.user_ref_id,
                        ca_users.user_first_name as donor_fname,
                        ca_users.user_last_name as donor_lname,
                        ca_users.user_mobile as donor_phone,
                        ca_users.user_home_phone  as donor_homePhone,
                        ca_users.user_email  as donor_donarmail,
                        ca_users.user_add_cont  as donor_country,
                        ca_users.user_add_state  as donor_state,
                        ca_users.user_add_post_code  as donor_pocode,
                        ca_users.user_add_city  as donor_city,
                        ca_users.user_add_st as donor_street
                        FROM
                        ca_recp_donor
                        INNER JOIN ca_recipient ON ca_recp_donor.recp_id = ca_recipient.recp_id
                        INNER JOIN ca_recp_types ON ca_recipient.recp_type_id = ca_recp_types.recp_type_id
                        INNER JOIN ca_org_users ON ca_recp_donor.ou_id = ca_org_users.ou_id
                        INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
                        WHERE
                        ca_recipient.org_id = :orgid";
        try
		{
			$stmt = $db->prepare($getProjects);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Recipents Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //get the ist of type of rcep Used when new rec is being created
    public function GetTypeList ($request , $response){

        $db = getDB();
        $getAll = "SELECT
            ca_recp_types.recp_type_id,
            ca_recp_types.recp_type_name
            FROM
            ca_recp_types";

        try
        {
            $stmt = $db->prepare($getAll);
            $stmt->execute();
            $projects = $stmt->fetchAll();
            $db = null;
            $max = sizeof($projects);
            //echo '{"result":'.json_encode($projects).'}';
            if($max > 0){
            //returning response back
            return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
            ->write('{"result":'.json_encode($projects).'}'); 
            }else{
                $data = array('access' => 'forbidden', 'msg' => 'No Recipents Currently Active', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }

        }
        catch (PDOException $exception)
        {
            echo '{"error":{"result":'. $exception->getMessage() .'}}';
        }
    }

    //get he recp detail by the refer id of recp
    public function GetRecpDetailsOnRefID(){
        
    }

    //get single rec on recp id
    public function GetRecpById(){
        
    }
    
    //update the recp details
    public function UpdateRecpDetailsyRecpId(){
        
    }

    //get recp by nationa id number
    public function GetRecpByNationalID(){
        
    }


    // HERE WE ARE GOING THE ASSING THE PROJECTS TO RECP
    
    //assing a recpt to donor
    public function AssingDonor($request , $response){
        $data = $request->getParsedBody();
		
        $recp_id = $data['recp_id'];
        $ou_id = $data['ou_id'];
    
        $db = getDB();

        $insertStatement = $db->insert(array('recp_id', 'ou_id'))
			->into('ca_recp_donor')
            ->values(array($recp_id, $ou_id));

            $insertId = $insertStatement->execute(false);
            $recp_id = $db->lastInsertId();
           
            if($insertId){
                $data = array('creation' => 'Success', 'msg' => 'Receipent has been Assinged to a Donor', 'status' => 201 , 'id' => $id);
                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
            }else{
                $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
    }



    //request for a receipited by a donor from mobile app
    public function RequestReceipeted($request , $response){
        $data = $request->getParsedBody();
		
        $amount_donated = $data['amount_donated'];
        $ou_id = $data['ou_id'];
    
        $db = getDB();

        $insertStatement = $db->insert(array( 'ou_id' , 'amount_donated'))
			->into('ca_users_request')
            ->values(array($ou_id, $amount_donated));

            $insertId = $insertStatement->execute(false);
            $recp_id = $db->lastInsertId();
           
            if($insertId){
                $data = array('creation' => 'Success', 'msg' => 'Your Request has been sent to Admin', 'status' => 201 , 'id' => $id);
                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
            }else{
                $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
    }

    //here we get the requests for rceipets from app users
    public function GetRequestsForAdmin($request , $response){
        $db = getDB();
        $approvecode = $request->getAttribute('approvecode');
        $orgid = $request->getAttribute('orgid');
        

        $getProjects = "SELECT
                            ca_users_request.ou_id,
                            ca_users.user_first_name,
                            ca_users.user_last_name,
                            ca_users.user_mobile,
                            ca_users.user_home_phone,
                            ca_users.user_email,
                            ca_users.user_add_cont,
                            ca_users.user_add_state,
                            ca_users.user_add_city,
                            ca_users.user_add_post_code,
                            ca_users.user_add_st,
                            ca_users_request.amount_donated
                            FROM
                            ca_users_request
                            INNER JOIN ca_org_users ON ca_users_request.ou_id = ca_org_users.ou_id
                            INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
                            WHERE
                            ca_users_request.is_approved = :approvecode AND
                            ca_org_users.org_id = :orgid";
        try
		{
            $stmt = $db->prepare($getProjects);
            $stmt->bindParam("approvecode", $approvecode);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Recipents Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //get list of family for a donor by OUID
    public function GetFamilyOUID(){
    }


    public function CreateRecpActivity($request , $response){

        //getting the request
        $data = $request->getParsedBody();
       
        $recp_id = $data['recp_id'];
        $actname = $data['actname'];
        $actdetail = $data['actdetail'];
        $filename = "";

        //check the file type
        $type = $_FILES['uploads']['type'];
        if($type == "image/jpeg" || $type == "image/png"){  
            
            //getting the upload directory
            $uploaddir =  __DIR__ . '/uploads/';


            //getting the directory to file to transfer
            $uploadfile = $uploaddir . basename($_FILES['uploads']['name']);


            //getting the image name 
            $userImageName = basename($_FILES['uploads']['name']);

            //saving the file on the server
            if (move_uploaded_file($_FILES['uploads']['tmp_name'], $uploadfile)) 
            {
                //file saved successfully now save the details to the database 
                //get the db 
                $db = getDB();

                //insert query
                $insertStatement = $db->insert(array('recp_id', 'recp_act_name', 'recp_act_detail', 'recp_act_link'))
                    ->into('ca_recp_activities')
                    ->values(array($recp_id, $actname, $actdetail, $userImageName));
                
                //executing the statement
                $insertId = $insertStatement->execute(false);
                $db = null;
                if($insertId){
                    $data = array('upload' => 'Success', 'msg' => 'Your Activity has been uploaded successfully.', 'status' => 201);
                    return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));   
                }else{
                    $data = array('upload' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                    return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                }
            }
            //file not saved to server
            else
            {
                $data = array('upload' => 'Failed', 'msg' => 'Unable to upload your picture. Please try again', 'status' => 415);
                return $response->withStatus(415)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }

        }
        else{
            $data = array('upload' => 'Failed', 'msg' => 'You have chosen a wrong File. We support JPG/JPEG and PNG only.', 'status' => 400);
            return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }


    public function GetSingleRecepWeb($request , $response)
	{
		$db = getDB();
				
			//getting the request body
			$recpid = $request->getAttribute('recpid');
		
			//user basic info
			$getRecp = "SELECT 
							ca_recipient.org_id,
							ca_recipient.recp_refid,
							ca_recipient.recp_refForms,
							ca_recipient.recp_type_id,
							ca_recipient.recp_firstName,
							ca_recipient.recp_lastName,
							(Select TIMESTAMPDIFF(Year,ca_recipient.recp_dob, NOW())) as recp_dob,
							ca_recipient.recp_gender,
                            ca_recipient.recp_class,
							ca_recipient.recp_status,
							ca_recipient.recp_category,
							ca_recipient.recp_country,
							ca_recipient.recp_state,
							ca_recipient.recp_postcode,
							ca_recipient.recp_city,
							ca_recipient.recp_street,
							ca_recipient.recp_area,
							ca_recipient.recp_mobileNo,
							ca_recipient.recp_homePhone,
							ca_recipient.recp_email,
							ca_recipient.recp_nationalId
							FROM
                            ca_recipient  WHERE ca_recipient.recp_id = :recpid";
                            
            $getRecpAt = "SELECT
                    ca_recp_activities.recp_act_id,
                    ca_recp_activities.recp_id,
                    ca_recp_activities.recp_act_name,
                    ca_recp_activities.recp_act_detail,
                    ca_recp_activities.recp_act_link
                    FROM
                    ca_recp_activities WHERE ca_recp_activities.recp_id = :recpid";

                            
		
		try
			{
				//EXECUTE Recp
				$stmt = $db->prepare($getRecp);
				$stmt->bindParam("recpid", $recpid);
				$stmt->execute();				
				$recp = $stmt->fetchAll();
				
				//EXECUTE RecpActis
				$stmts = $db->prepare($getRecpAt);
				$stmts->bindParam("recpid", $recpid);
				$stmts->execute();				
				$activites = $stmts->fetchAll();

				
				// $db = null;

				 $ar1 = array("info" =>$recp);
				 $ar2 = array("activites" => $activites);
	
				 $result = array_merge_recursive($ar1, $ar2);
				// $ar3 = array("recp" => $recps);

				// $finalresult = array_merge_recursive($result, $ar3);

				// $ar4 = array("requests" => $userrequests);
				// $lastfinalresult = array_merge_recursive($finalresult, $ar4);
				$max = sizeof($recp);
				//echo '{"result":'.json_encode($users).'}';
				if($max > 0){
				//returning response back
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
				->write('{"result":'.json_encode($result).'}'); 
				}else{
					$data = array('access' => 'forbidden', 'msg' => 'Your  details are incorrect', 'status' => 400);
					return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
				}
			
			}
			catch (PDOException $exception)
			{
				echo '{"error":{"result":'. $exception->getMessage() .'}}';
			}
    }
    

    public function GetSingleRecpPhones($request , $response)
	{
		$db = getDB();
				
			//getting the request body
			$recpid = $request->getAttribute('recpid');
	
                            
            $getRecpAt = "SELECT
                    ca_recp_activities.recp_act_id,
                    ca_recp_activities.recp_id,
                    ca_recp_activities.recp_act_name,
                    ca_recp_activities.recp_act_detail,
                    ca_recp_activities.recp_act_link
                    FROM
                    ca_recp_activities WHERE ca_recp_activities.recp_id = :recpid";

                            
		
		try
			{
				
				//EXECUTE RecpActis
				$stmts = $db->prepare($getRecpAt);
				$stmts->bindParam("recpid", $recpid);
				$stmts->execute();				
				$activites = $stmts->fetchAll();

				
			
				$max = sizeof($activites);
				//echo '{"result":'.json_encode($users).'}';
				if($max > 0){
				//returning response back
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
				->write('{"result":'.json_encode($activites).'}'); 
				}else{
					$data = array('access' => 'forbidden', 'msg' => 'Your  details are incorrect', 'status' => 400);
					return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
				}
			
			}
			catch (PDOException $exception)
			{
				echo '{"error":{"result":'. $exception->getMessage() .'}}';
			}
	}


}
?>