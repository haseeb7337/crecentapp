<?php
namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;


class UserController{
    
    //test function
    public function HomeTest(){
        echo "Hello form user";
    }
    //creating a new user  Data has to be added to org_user table as well
    //when user is registered
    public function RegisterNewUser($request , $response){

		$data = $request->getParsedBody();
		
		$user_ref_id = $data['user_ref_id'];
		$registrar_id = $data['registrar_id'];
		$user_first_name = $data['user_first_name'];
		$user_last_name = $data['user_last_name'];
		$user_mobile = $data['user_mobile'];
		$user_home_phone = $data['user_home_phone'];
		$user_email = $data['user_email'];
		$user_add_cont = $data['user_add_cont'];
		$user_add_state = $data['user_add_state'];
		$user_add_post_code = $data['user_add_post_code'];
		$user_add_city = $data['user_add_city'];
		$user_add_st = $data['user_add_st'];
		$user_password = $data['user_password'];
		$org_id = $data['org_id'];
		
		$db = getDB();

		//checking if email is already used
		$selectStatement = $db->select(array('user_id'))->from('ca_users')->where('user_email', '=', $user_email);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		$size = sizeof($data);

		if($size > 0)
		{
			$data = array('registration' => 'Failed', 'msg' => 'Email already exits. Want to try with different email?', 'status' => 409);
			return $response->withStatus(409)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
		}

		else
		{
			$insertStatement = $db->insert(array('user_ref_id', 'registrar_id', 'user_first_name' , 'user_last_name', 'user_mobile',
			'user_email' , 'user_add_cont' , 'user_add_state' , 'user_add_post_code' , 'user_add_city' , 'user_add_st', 'user_password'))
			->into('ca_users')
			->values(array($user_ref_id, $registrar_id, $user_first_name , $user_last_name, $user_mobile, $user_email , $user_add_cont , $user_add_state , $user_add_post_code , $user_add_city , $user_add_st , $user_password));
			
			$insertId = $insertStatement->execute(false);
			$id = $db->lastInsertId();
			if($insertId){
				$insertStatement = $db->insert(array('org_id' , 'user_id'))->into('ca_org_users')
				->values(array($org_id , $id));

				$insertOrgUser = $insertStatement->execute(false);
				$user_ouid = $db->lastInsertId();
				if($insertOrgUser){
					
					$insertStatement = $db->insert(array('ou_id' , 'role_org_id'))->into('ca_org_user_role')
					->values(array($user_ouid , "1"));
					$save_roles = $insertStatement->execute(false);
					if($save_roles){
						$data = array('registration' => 'Success', 'msg' => 'Your account has been registered successfully.', 'status' => 201 , 'id' => $id);
						return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
					}else{
						$data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
						return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
					}
					
					  
				}else{
					$data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
					return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
				} 
			}//main insert if ends here
			else{
				$data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
				return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
			}//linked to above if main isnert


		}
    }

    //login user
    public function UserLogin($request , $response){
		$db = getDB();
            
        //getting the request body
		$data = $request->getParsedBody();
		$email = $data['email'];
		$password = $data['password'];
		
		/*$loginUser = "SELECT
					ca_users.user_id,
					ca_users.user_first_name,
					ca_users.user_last_name,
					ca_users.user_mobile,
					ca_users.user_add_cont,
					ca_users.user_add_state,
					ca_users.user_add_post_code,
					ca_users.user_add_city,
					ca_users.user_add_st,
					(SELECT ou_id from ca_org_users where ca_org_users.user_id = ca_users.user_id) AS ou_id,
					(SELECT ca_org.org_id FROM ca_org INNER JOIN ca_org_users ON ca_org_users.org_id = ca_org.org_id WHERE ca_org_users.user_id = ca_users.user_id) as org_id,
					(SELECT
					ca_org_roles.role_name
					FROM
					ca_org_roles
					INNER JOIN ca_org_user_role ON ca_org_user_role.role_org_id = ca_org_roles.role_org_id
					WHERE
					ca_org_roles.org_id = (SELECT ou_id from ca_org_users where ca_org_users.user_id = ca_users.user_id) AND
					ca_org_user_role.ou_id = (SELECT ca_org.org_id FROM ca_org INNER JOIN ca_org_users ON ca_org_users.org_id = ca_org.org_id WHERE ca_org_users.user_id = ca_users.user_id)) as user_role
					FROM
					ca_users
					WHERE
					ca_users.user_email = :email AND
					ca_users.user_password = :password";*/
		$loginUser = "SELECT
		ca_org_users.user_id,
		ca_users.user_ref_id,
		ca_users.registrar_id,
		ca_users.user_first_name,
		ca_users.user_last_name,
		ca_users.user_mobile,
		ca_users.user_home_phone,
		ca_users.user_email,
		ca_users.user_add_cont,
		ca_users.user_add_state,
		ca_users.user_add_post_code,
		ca_users.user_add_city,
		ca_users.user_add_st,
		ca_users.account_status,
		ca_org_user_role.ou_id,
		ca_org_users.org_id,
		ca_org_roles.role_name
		FROM
		ca_org_roles
		INNER JOIN ca_org_user_role ON ca_org_user_role.role_org_id = ca_org_roles.role_org_id
		INNER JOIN ca_org_users ON ca_org_user_role.ou_id = ca_org_users.ou_id AND ca_org_roles.org_id = ca_org_users.org_id
		INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
		WHERE
		ca_users.user_email = :email AND
		ca_users.user_password = :password";
		try
		{
			$stmt = $db->prepare($loginUser);
			$stmt->bindParam("email", $email);
			$stmt->bindParam("password", $password);
			$stmt->execute();
			$users = $stmt->fetchAll();
			$db = null;
			$max = sizeof($users);
			//echo '{"result":'.json_encode($users).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($users).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'Your login details are incorrect', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //get list of users for a current organizaton.
    //data will be selected from the org_user table based on org_id
    public function getAllOrgUsers($request , $response){
		
		//getting the parameters 
		$orgid = $request->getAttribute('orgid'); 
	
		//get the db 
		$db = getDB();
		//the query
		$selectStatement = $db->select(array('ca_org_users.ou_id', 'ca_users.user_id' , 'ca_users.user_ref_id', 'ca_users.user_first_name' , 'ca_users.user_last_name' , 'ca_users.user_add_cont', 'ca_users.user_add_city', 'ca_org.org_display_name'))->from('ca_org_users')
		->join('ca_org' , 'ca_org_users.org_id', '=' , 'ca_org.org_id' , 'INNER')
		->join('ca_users' , 'ca_org_users.user_id', '=' , 'ca_users.user_id' , 'INNER')
		->where('ca_org.org_id' , '=', $orgid);
		//->whereMany(array('tbl_wallpapers.cat_id' => $categoryId, 'is_image_approv' => $status), '=');;

		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();

	   // print_r($data);
		$size = sizeof($data);
		if($size > 0)
		{
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($data).'}'); 
		}
		else
		{
			$data = array('msg' => 'No Wallpapers found', 'status' => 400);
			return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
		}  
        
    }

    //when user registered for the new account, this query
    // will be used to update his account status
    // we are also using this query to make other updates as required
    public function UpdateAccountStatus($request , $response){
		$db = getDB();
		//getting the get varibales form the request
		//$allGetVars = $request->getQueryParams();
		$user_id = $request->getAttribute('userid');
		$account_status = $request->getAttribute('usrstatus');

	  
		//update query for the user unblock
		$updateStatement = $db->update(array('account_status' => $account_status))
				   ->table('ca_users')
				   ->where('user_id', '=', $user_id);
		$affectedRows = $updateStatement->execute();
		$db = null;
		if($affectedRows){
			$data = array('msg' => 'User status changed', 'status' => 200);
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
		}else{
			$data = array('msg' => 'Unable to update the status. Try again later', 'status' => 400);
			return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
		}
	}
	

	//get a sinlge org user
	public function getSingleUser($request , $response){
	
			$db = getDB();
				
			//getting the request body
			$ouid = $request->getAttribute('ouid');
			//$userid = $request->getAttribute('userid');
			
			$loginUser = "SELECT
						ca_org_users.user_id,
						ca_users.user_ref_id,
						ca_users.registrar_id,
						ca_users.user_first_name,
						ca_users.user_last_name,
						ca_users.user_mobile,
						ca_users.user_home_phone,
						ca_users.user_email,
						ca_users.user_add_cont,
						ca_users.user_add_state,
						ca_users.user_add_post_code,
						ca_users.user_add_city,
						ca_users.user_add_st,
						ca_users.account_status,
						ca_org_user_role.ou_id,
						ca_org_users.org_id,
						ca_org_roles.role_name
						FROM
						ca_org_roles
						INNER JOIN ca_org_user_role ON ca_org_user_role.role_org_id = ca_org_roles.role_org_id
						INNER JOIN ca_org_users ON ca_org_user_role.ou_id = ca_org_users.ou_id AND ca_org_roles.org_id = ca_org_users.org_id
						INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
						WHERE
						ca_org_users.ou_id = :ouid";
			try
			{
				$stmt = $db->prepare($loginUser);
				$stmt->bindParam("ouid", $ouid);
				//$stmt->bindParam("userid", $userid);
				$stmt->execute();
				$users = $stmt->fetchAll();
				$db = null;
				$max = sizeof($users);
				//echo '{"result":'.json_encode($users).'}';
				if($max > 0){
				//returning response back
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
				->write('{"result":'.json_encode($users).'}'); 
				}else{
					$data = array('access' => 'forbidden', 'msg' => 'Your  details are incorrect', 'status' => 400);
					return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
				}
			
			}
			catch (PDOException $exception)
			{
				echo '{"error":{"result":'. $exception->getMessage() .'}}';
			}
		
	}


    //in case user forgets his password.
    public function ResetPassword(){

	}
	
	//submit feedback...!!
	public function SubmitFeedBackUser($request,  $response)
	{
		$data = $request->getParsedBody();
		
		$ou_id = $data['ou_id'];
		$feedback_text = $data['feedback_text'];
		
		
		$db = getDB();
		$insertStatement = $db->insert(array('ou_id', 'feedback_text'))
							  ->into('ca_users_feedback')
							  ->values(array($ou_id, $feedback_text));
		$insertId = $insertStatement->execute(false);
		if($insertId){
			$data = array('registration' => 'Success', 'msg' => 'Your Feedback has been sent successfully.', 'status' => 201 , 'id' => $id);
			return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
		}else{
			$data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
			return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
		}
	}

	public function testmutil($request , $response){
		$data = $request->getParsedBody();
		foreach($data as $item) {
			//echo $item['mContactID']." ".$item['mDisplayName']." ".$item['mDisplayName']."\n";
			//echo $item['mContactID']." ".$item['mDisplayName'];
			//echo sizeof( $item['mEmails']);

			if(sizeof( $item['mEmails']) ==  0){
				echo $item['mContactID']." ".$item['mDisplayName']." ". $item['mPhoneNumbers']['0']."\n";
			}
			else{
				echo $item['mContactID']." ".$item['mDisplayName']." ". $item['mEmails']['0']."\n";
			}
		}

		//$characters = json_decode("", $data); // decode the JSON feed
		// foreach ($characters as $character) {
		// 	echo $character->mDisplayName . '<br>';
		// }


		//print_r ($data['mContactID']);
		// foreach($data['results'] as $item) {
		// 	//echo $item['name']." ".$item['email']." ".$item['phone']."\n";
		// 	if($item['email'] != ""){
		// 		echo "Send Email\n";
		// 	}else{
		// 		echo "Send SMS\n";
		// 	}
		
		// 	//echo 'Title: ' . $item['product']['title'] . '<br />';
		// 	//echo 'Brand: ' . $item['product']['brand'] . '<br />';
		
		// }
		//$ou_id = $data['model'];
		//echo $data.length();
	}

	public function GetDonors($request , $response){
		$db = getDB();	
			$loginUser = "SELECT
			ca_org_users.user_id,
			ca_users.user_ref_id,
			ca_users.registrar_id,
			ca_users.user_first_name,
			ca_users.user_last_name,
			ca_users.user_mobile,
			ca_users.user_home_phone,
			ca_users.user_email,
			ca_users.user_add_cont,
			ca_users.user_add_state,
			ca_users.user_add_post_code,
			ca_users.user_add_city,
			ca_users.user_add_st,
			ca_users.account_status,
			ca_org_user_role.ou_id,
			ca_org_users.org_id,
			ca_org_roles.role_name
			FROM
					ca_org_roles
					INNER JOIN ca_org_user_role ON ca_org_user_role.role_org_id = ca_org_roles.role_org_id
					INNER JOIN ca_org_users ON ca_org_user_role.ou_id = ca_org_users.ou_id AND ca_org_roles.org_id = ca_org_users.org_id
					INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
			WHERE
			ca_org_roles.role_name = 'User'";
			try
			{
				$stmt = $db->prepare($loginUser);
				$stmt->bindParam("orgid", $orgid);
				$stmt->bindParam("userid", $userid);
				$stmt->execute();
				$users = $stmt->fetchAll();
				$db = null;
				$max = sizeof($users);
				//echo '{"result":'.json_encode($users).'}';
				if($max > 0){
				//returning response back
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
				->write('{"result":'.json_encode($users).'}'); 
				}else{
					$data = array('access' => 'forbidden', 'msg' => 'Your  details are incorrect', 'status' => 400);
					return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
				}
			
			}
			catch (PDOException $exception)
			{
				echo '{"error":{"result":'. $exception->getMessage() .'}}';
			}
	}


	//get a sinlge org user
	public function GetUserDetails($request , $response){
	
			$db = getDB();
				
			//getting the request body
			$ouid = $request->getAttribute('ouid');
			$orgid = $request->getAttribute('orgid');
			
			//user basic info
			$loginUser = "SELECT
						ca_org_users.user_id,
						ca_users.user_ref_id,
						ca_users.registrar_id,
						ca_users.user_first_name,
						ca_users.user_last_name,
						ca_users.user_mobile,
						ca_users.user_home_phone,
						ca_users.user_email,
						ca_users.user_add_cont,
						ca_users.user_add_state,
						ca_users.user_add_post_code,
						ca_users.user_add_city,
						ca_users.user_add_st,
						ca_users.account_status,
						ca_org_user_role.ou_id,
						ca_org_users.org_id,
						ca_org_roles.role_name
						FROM
						ca_org_roles
						INNER JOIN ca_org_user_role ON ca_org_user_role.role_org_id = ca_org_roles.role_org_id
						INNER JOIN ca_org_users ON ca_org_user_role.ou_id = ca_org_users.ou_id AND ca_org_roles.org_id = ca_org_users.org_id
						INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
						WHERE
						ca_org_users.ou_id = :ouid";
			//user networks
			$getNetwork = "SELECT
					ca_users_network.network_phone,
					ca_users_network.network_email,
					ca_users_network.network_firstname
					FROM
					ca_users_network
					INNER JOIN ca_org_users ON ca_users_network.ou_id = ca_org_users.ou_id
					WHERE
					ca_org_users.org_id = :orgid  AND
					ca_users_network.ou_id = :ouid";

			//user family
			$getFamily = "SELECT
						ca_recipient.recp_id,
						ca_recipient.recp_refid,
						ca_recipient.recp_refForms,
						ca_recipient.recp_firstName,
						ca_recipient.recp_lastName,
						(Select TIMESTAMPDIFF(Year,ca_recipient.recp_dob, NOW())) as recp_dob,
						ca_recipient.recp_gender,
						ca_recipient.recp_class,
						ca_recipient.recp_status,
						ca_recipient.recp_category,
						ca_recipient.recp_country,
						ca_recipient.recp_state,
						ca_recipient.recp_postcode,
						ca_recipient.recp_city,
						ca_recipient.recp_street,
						ca_recipient.recp_area,
						ca_recipient.recp_mobileNo,
						ca_recipient.recp_homePhone,
						ca_recipient.recp_email,
						ca_recipient.recp_nationalId
						FROM
						ca_recp_donor
						INNER JOIN ca_recipient ON ca_recp_donor.recp_id = ca_recipient.recp_id
						WHERE
						ca_recp_donor.ou_id = :ouid AND
						ca_recipient.org_id = :orgid";

			//user requests
			$getRequests = "SELECT
							ca_users_request.ou_id,
							ca_users.user_first_name,
							ca_users.user_last_name,
							ca_users.user_mobile,
							ca_users.user_home_phone,
							ca_users.user_email,
							ca_users.user_add_cont,
							ca_users.user_add_state,
							ca_users.user_add_city,
							ca_users.user_add_post_code,
							ca_users.user_add_st,
							ca_users_request.amount_donated
							FROM
							ca_users_request
							INNER JOIN ca_org_users ON ca_users_request.ou_id = ca_org_users.ou_id
							INNER JOIN ca_users ON ca_org_users.user_id = ca_users.user_id
							WHERE
							ca_users_request.is_approved = '0' AND
							ca_org_users.org_id = :orgid AND ca_users_request.ou_id = :ouid";

			//user recipients
			try
			{
				//EXECUTE USERS
				$stmt = $db->prepare($loginUser);
				$stmt->bindParam("ouid", $ouid);
				$stmt->execute();				
				$users = $stmt->fetchAll();
				
				//EXECUTE NETWORKS
				$stmts = $db->prepare($getNetwork);
				$stmts->bindParam("ouid", $ouid);
				$stmts->bindParam("orgid", $orgid);
				$stmts->execute();				
				$networks = $stmts->fetchAll();

				//EXECUTE Recipets
				$stmtr = $db->prepare($getFamily);
				$stmtr->bindParam("ouid", $ouid);
				$stmtr->bindParam("orgid", $orgid);
				$stmtr->execute();				
				$recps = $stmtr->fetchAll();


				//EXECUTE requests
				$stmtrs = $db->prepare($getRequests);
				$stmtrs->bindParam("ouid", $ouid);
				$stmtrs->bindParam("orgid", $orgid);
				$stmtrs->execute();				
				$userrequests = $stmtrs->fetchAll();
				$db = null;

				$ar1 = array("userprofile" =>$users);
				$ar2 = array("usernetwork" => $networks);
	
				$result = array_merge_recursive($ar1, $ar2);
				$ar3 = array("recp" => $recps);

				$finalresult = array_merge_recursive($result, $ar3);

				$ar4 = array("requests" => $userrequests);
				$lastfinalresult = array_merge_recursive($finalresult, $ar4);
				$max = sizeof($users);
				//echo '{"result":'.json_encode($users).'}';
				if($max > 0){
				//returning response back
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
				->write('{"result":'.json_encode($lastfinalresult).'}'); 
				}else{
					$data = array('access' => 'forbidden', 'msg' => 'Your  details are incorrect', 'status' => 400);
					return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
				}
			
			}
			catch (PDOException $exception)
			{
				echo '{"error":{"result":'. $exception->getMessage() .'}}';
			}
		
	}

	
}

?>