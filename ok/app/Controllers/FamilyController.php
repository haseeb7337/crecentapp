<?php
namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;

class FamilyController{

    public function GetMyFamilyMembers($request , $response){
        $db = getDB();
        $ouid = $request->getAttribute('ouid');
        $orgid = $request->getAttribute('orgid');

        $getProjects = "SELECT
                    ca_recipient.recp_id,
                    ca_recipient.recp_refid,
                    ca_recipient.recp_refForms,
                    ca_recipient.recp_type_id,
                    ca_recipient.recp_firstName,
                    ca_recipient.recp_lastName,
                    ca_recipient.recp_dob,
                    ca_recipient.recp_gender,
                    ca_recipient.recp_class,
                    ca_recipient.recp_status,
                    ca_recipient.recp_category,
                    ca_recipient.recp_country,
                    ca_recipient.recp_state,
                    ca_recipient.recp_postcode,
                    ca_recipient.recp_city,
                    ca_recipient.recp_street,
                    ca_recipient.recp_area,
                    ca_recipient.recp_mobileNo,
                    ca_recipient.recp_homePhone,
                    ca_recipient.recp_email,
                    ca_recipient.recp_nationalId
                    FROM
                    ca_recp_donor
                    INNER JOIN ca_recipient ON ca_recp_donor.recp_id = ca_recipient.recp_id
                    WHERE
                    ca_recp_donor.ou_id = :ouid AND
                    ca_recipient.org_id = :orgid";
        try
		{
            $stmt = $db->prepare($getProjects);
            $stmt->bindParam("ouid", $ouid);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'You do not have any Family Members', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

}

?>