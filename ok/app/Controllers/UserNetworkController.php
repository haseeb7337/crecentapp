<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;

class UserNetworkController{

    public function CheckTest(){
        echo "Hello Network....!!";
    }


    //adding a new network to the system.
    //first it will be checked from the database 
    //then add to the system if network email does not exists
    public function AddNewDetailsNetwork($request , $response){
        $data = $request->getParsedBody();

        $ouid = $request->getAttribute('ouid');

        

        $db = getDB();
		foreach($data as $item) {
			//echo $item['mContactID']." ".$item['mDisplayName']." ".$item['mDisplayName']."\n";
			//echo $item['mContactID']." ".$item['mDisplayName'];
			//echo sizeof( $item['mEmails']);

            //send sms here
			if(sizeof( $item['mPhoneNumbers']) !=  0){
                //echo $item['mContactID']." ".$item['mDisplayName']." ". $item['mPhoneNumbers']['0']."\n";
                $insertStatement = $db->insert(array('ou_id', 'network_phone', 'network_email', 'network_firstname'))
			        ->into('ca_users_network')
                    ->values(array($ouid , $item['mPhoneNumbers']['0'], '', $item['mDisplayName']));
                $insertId = $insertStatement->execute(false);
            }
        
			else if(sizeof( $item['mEmails']) !=  0){
                //echo $item['mContactID']." ".$item['mDisplayName']." ". $item['mEmails']['0']."\n";
                $insertStatement = $db->insert(array('ou_id', 'network_phone', 'network_email', 'network_firstname'))
			        ->into('ca_users_network')
                    ->values(array($ouid , '', $item['mEmails']['0'], $item['mDisplayName']));
                $insertId = $insertStatement->execute(false);
            }
            else
            {

            }
        }
        
        $data = array('creation' => 'Success', 'msg' => 'Contact Invitation Received', 'status' => 201);
        return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
       
    }



    public function ListUserNetwork($request , $response){
        $db = getDB();

        $orgid = $request->getAttribute('orgid');
        $ouid = $request->getAttribute('ouid');
    
        $getProjects = "SELECT
                        ca_users_network.network_phone,
                        ca_users_network.network_email,
                        ca_users_network.network_firstname
                        FROM
                        ca_users_network
                        INNER JOIN ca_org_users ON ca_users_network.ou_id = ca_org_users.ou_id
                        WHERE
                        ca_org_users.org_id = :orgid  AND
                        ca_users_network.ou_id = :ouid";
        try
		{
			$stmt = $db->prepare($getProjects);
            $stmt->bindParam("orgid", $orgid);
            $stmt->bindParam("ouid", $ouid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Vouchers Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }



    public function AddNetworkIOS($request , $response){
        $data = $request->getParsedBody();
        $ouid = $request->getAttribute('ouid');



        $data = array('creation' => 'Success', 'msg' => 'Contact Invitation Received', 'status' => 201);
        return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
    }




    //we will check the network details before adding the new contact
    public function CheckNetworkDetailsDB(){
    }

    //get list of users network by ou_id 
    public function GetUserNetworkDetails(){
        
    }

    

}
?>