<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;

class ProjectsController{

    public function TestProject(){
        echo "Success Project...!!!";
    }


    //create a new project
    public function CreateNewProject($request , $response){

        $data = $request->getParsedBody();
		
		$org_id = $data['org_id'];
		$project_type_id = $data['project_type_id'];
		$project_name = $data['project_name'];
		$project_breif = $data['project_breif'];
		$project_details = $data['project_details'];
        $project_logo = $data['project_logo'];
        $status_id = $data['status_id'];
        $project_start_date = $data['project_start_date'];
        $project_end_date = $data['project_end_date'];
        $db = getDB();
        
        $insertStatement = $db->insert(array('org_id', 'project_type_id', 'project_name' , 'project_breif', 'project_details','project_logo' , 'project_start_date' , 'project_end_date', 'status_id'))
        ->into('ca_projects')
        ->values(array($org_id, $project_type_id, $project_name , $project_breif, $project_details, $project_logo, $project_start_date, $project_end_date,  $status_id));
        $insertId = $insertStatement->execute(false);
        $id = $db->lastInsertId();
        if($insertId){
            $data = array('registration' => 'Success', 'msg' => 'New Project has been Created.', 'status' => 201 , 'id' => $id);
            return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
        }else{
            $data = array('creation' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
				return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        }
       
        
    }

    //get all projects by org id for admin
    public function GetAllProjectsByOrgId($request , $response){
        
        $db = getDB();

        $orgid = $request->getAttribute('orgid');
       
        $getAdminProjects = "SELECT
                        ca_projects.project_id,
                        ca_projects.project_type_id,
                        ca_project_types.project_type_name,
                        ca_projects.project_name,
                        ca_projects.project_breif,
                        ca_projects.project_details,
                        ca_projects.project_logo,
                        ca_projects.project_start_date,
                        ca_projects.project_end_date,
                        ca_status_standard.status_name AS project_status
                        FROM
                        ca_projects
                        INNER JOIN ca_status_standard ON ca_projects.status_id = ca_status_standard.status_id
                        INNER JOIN ca_project_types ON ca_projects.project_type_id = ca_project_types.project_type_id
                        WHERE
                        ca_projects.org_id = :orgid";
        try
		{
			$stmt = $db->prepare($getAdminProjects);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Project Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
        
    }

    //get project on project id or admid
    public function GetProjectByProjectId($request , $response){
        $db = getDB();

        $orgid = $request->getAttribute('orgid');
        $projectid = $request->getAttribute('projectid');
        $getSingleProject = "SELECT
                            ca_projects.project_id,
                            ca_projects.project_type_id,
                            ca_project_types.project_type_name,
                            ca_projects.project_name,
                            ca_projects.project_breif,
                            ca_projects.project_details,
                            ca_projects.project_logo,
                            ca_projects.project_start_date,
                            ca_projects.project_end_date,
                            ca_status_standard.status_name AS project_status
                            FROM
                            ca_projects
                            INNER JOIN ca_status_standard ON ca_projects.status_id = ca_status_standard.status_id
                            INNER JOIN ca_project_types ON ca_projects.project_type_id = ca_project_types.project_type_id
                            WHERE
                            ca_projects.org_id = :orgid AND ca_projects.project_id = :projectid";

            try
            {
                $stmt = $db->prepare($getSingleProject);
                $stmt->bindParam("orgid", $orgid);
                $stmt->bindParam("projectid", $projectid);
                $stmt->execute();
                $projects = $stmt->fetchAll();
                $db = null;
                $max = sizeof($projects);
                //echo '{"result":'.json_encode($projects).'}';
                if($max > 0){
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
                ->write('{"result":'.json_encode($projects).'}'); 
                }else{
                    $data = array('access' => 'forbidden', 'msg' => 'No Project Currently Active', 'status' => 400);
                    return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
                }

            }
            catch (PDOException $exception)
            {
                echo '{"error":{"result":'. $exception->getMessage() .'}}';
            }
    }

    //get active projects for user
    public function GetActiveProjectsUser($request , $response){
        $db = getDB();

        $orgid = $request->getAttribute('orgid');
        $status = $request->getAttribute('status');
        
        $getProjects = "SELECT
                        ca_projects.project_id,
                        ca_projects.project_type_id,
                        ca_project_types.project_type_name,
                        ca_projects.project_name,
                        ca_projects.project_breif,
                        ca_projects.project_details,
                        ca_projects.project_logo,
                        ca_projects.project_start_date,
                        ca_projects.project_end_date,
                        ca_status_standard.status_name AS project_status
                        FROM
                        ca_projects
                        INNER JOIN ca_status_standard ON ca_projects.status_id = ca_status_standard.status_id
                        INNER JOIN ca_project_types ON ca_projects.project_type_id = ca_project_types.project_type_id
                        WHERE
                        ca_projects.org_id = :orgid AND
                        ca_status_standard.status_name = 'Active'";
        try
		{
			$stmt = $db->prepare($getProjects);
			$stmt->bindParam("orgid", $orgid);
			$stmt->execute();
			$projects = $stmt->fetchAll();
			$db = null;
			$max = sizeof($projects);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($projects).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Project Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //get project status list for admin to add new projects
    public function GetStatusList($request , $response){
        $db = getDB();        
        $getProjects = "SELECT
                        ca_project_types.project_type_id,
                        ca_project_types.project_type_name
                        FROM
                        ca_project_types";

        $getstatus = "SELECT
                        ca_status_standard.status_id,
                        ca_status_standard.status_name
                        FROM
                        ca_status_standard";
        try
		{
			$stmt = $db->prepare($getProjects);
			$stmt->execute();
			$projects = $stmt->fetchAll();
		
            $max = sizeof($projects);

            $stmt_s = $db->prepare($getstatus);
			$stmt_s->execute();
			$status = $stmt_s->fetchAll();
			$db = null;
           
            

            $ar1 = array("status" =>$status);
            $ar2 = array("types" => $projects);

            $result = array_merge_recursive($ar1, $ar2);
			//echo '{"result":'.json_encode($projects).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($result).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'No Project Currently Active', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //admin action to update project status
    public function UpdateProjectStatus(){
        
    }


    //  ------------------------------------------------------------
    // ALl project activites are done here in Projct Activity Table
    //  ------------------------------------------------------------

    // add new project acitivity by project id
    public function AddNewProjectActivity(){

    }

    // get all project activittes  |  TO BE THOUGHT |
    public function GetProjectActivityForAdmin(){

    }

    //get project activity by project  id
    public function GetPorjectActivityById(){
        
    }

    
}
?>