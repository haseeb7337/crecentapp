<?php


// //calls related to user actions
// $app->get('/users' , 'UsersController:ListAllUsers');
// $app->post('/users/login' , 'UsersController:UserLogin');
// $app->post('/users/registration' , 'UsersController:UserRegistration');
// $app->put('/users/status/{userid}/{usrstatus}', 'UsersController:BlockUser');

// //cateogries relation actions
// $app->post('/categories/create' , 'CatController:CreateCate');
// $app->delete('/categories/delete/{catid}' , 'CatController:DeleteCat');
// $app->get('/categories/get/{status}' , 'CatController:GetApprovedCat');
// $app->patch('/categories/status/{catid}/{status}' , 'CatController:ApproveDisCat');


//  ---------------------------------------------------------------------------------------------------  //

//users Api Call
$app->get('/usertest' , 'UserController:HomeTest');
$app->post('/users/login' , 'UserController:UserLogin');
$app->get('/users/all/{orgid}' , 'UserController:getAllOrgUsers');
$app->put('/users/status/{userid}/{usrstatus}', 'UserController:UpdateAccountStatus');
$app->post('/users/register' , 'UserController:RegisterNewUser');
$app->get('/users/single/{ouid}' , 'UserController:getSingleUser');
$app->post('/users/feedback' , 'UserController:SubmitFeedBackUser');
$app->post('/users/testmutil' , 'UserController:testmutil');
$app->get('/users/getdonors' , 'UserController:GetDonors');
$app->get('/users/get/details/{orgid}/{ouid}' , 'UserController:GetUserDetails');


//organization api calls
$app->get('/orgtest' , 'OrganizationController:OrgTestM');

//feedback option api calls
$app->get('/feedtest' , 'FeedbackController:CheckFeedbackTest');

//user netork api calls
$app->get('/nettest' , 'UserNetworkController:CheckTest');
$app->post('/network/create/{ouid}' , 'UserNetworkController:AddNewDetailsNetwork');
$app->get('/network/get/{orgid}/{ouid}' , 'UserNetworkController:ListUserNetwork');
$app->post('/network/ios/{ouid}' , 'UserNetworkController:AddNetworkIOS');

//project api calls
$app->get('/protest' , 'ProjectsController:TestProject');
$app->get('/projects/active/{orgid}' , 'ProjectsController:GetActiveProjectsUser');
$app->get('/projects/admin/{orgid}' , 'ProjectsController:GetAllProjectsByOrgId');
$app->post('/projects/create' , 'ProjectsController:CreateNewProject');
$app->get('/projects/statuslist' , 'ProjectsController:GetStatusList');
$app->get('/projects/single/admin/{orgid}/{projectid}' , 'ProjectsController:GetProjectByProjectId');

//recipient api calls
$app->get('/recptest' , 'RecipientController:TestRecp');

//benifits api calls
$app->post('/benifits/create' , 'BenifitsController:CreateNewVoucher');
$app->get('/benifits/get/vouchers/{orgid}' , 'BenifitsController:ListAllVouchers');
$app->get('/benifits/get/assingedvouchers/{orgid}' , 'BenifitsController:GetAssingVoucherAdmin');
$app->post('/benifits/assingvoucher' , 'BenifitsController:AssingVoucher');
$app->get('/benifits/get/uservouchers/{ouid}/{isvoucheractive}/{orgid}' , 'BenifitsController:GetUserVoucher');
$app->get('/benifits/userlist/{orgid}/{roleid}' , 'BenifitsController:GetListUsersToAssing');
$app->post('/benifits/createorg' , 'BenifitsController:CreateCouponOrg');
$app->get('/benifits/sponserorg/{orgid}' , 'BenifitsController:GetSponserOrgs');
$app->get('/benifits/admincoupons/{orgid}' , 'BenifitsController:GetOrgVouchersAdim');
$app->get('/benifits/usercoupons/{locationName}/{orgid}' , 'BenifitsController:GetUserBenifits');
$app->post('/benifits/redeem' , 'BenifitsController:UpdateVouhcer');


//recept api call 
$app->post('/recp/create' , 'RecipientController:CreateNewRecp');
$app->get('/recp/get/{orgid}' , 'RecipientController:GetAllRecpByOrgID');
$app->get('/recp/types' , 'RecipientController:GetTypeList');
$app->get('/recp/assing/{orgid}' , 'RecipientController:GetAllAssingedRec');
$app->post('/recp/assing' , 'RecipientController:AssingDonor');
$app->post('/recp/request' , 'RecipientController:RequestReceipeted');
$app->get('/recp/userrequest/{approvecode}/{orgid}' , 'RecipientController:GetRequestsForAdmin');
$app->post('/recp/createactivity' , 'RecipientController:CreateRecpActivity');
$app->get('/recp/get/single/{recpid}' , 'RecipientController:GetSingleRecepWeb');
$app->get('/recp/get/activity/{recpid}' , 'RecipientController:GetSingleRecpPhones');


//family api calls
$app->get('/family/get/{ouid}/{orgid}' , 'FamilyController:GetMyFamilyMembers');


















?>